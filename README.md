# Modul 346 - Antliuma Aerospace Laboratories (AAL) - birdCloud
## Valentin Binotto

- [Dokumentation](/Dokumentationen/Project_birdCloud.md)
- [Über AAL und birdCloud](./corporate_docs/antliuma_aerospace_laboratories.md)
### Weisungen & Vorschriften
- [Weisung #AALD-DC-01 (Datenklassifizierung)](/corporate_docs/datenklassifizierung.md)
- [Weisung #AALD-PID-01 (Verarbeitung von Daten von Drittparteien)](/corporate_docs/thirddata.md)
- [Weisung #AALD-IAP-01 (Vergabe von Sicherheitsfreigaben)](/corporate_docs/securitylevel.md)
- [Weisung #AALD-TPA-01 (Transparenz gegenüber der Öffentlichkeit)](/corporate_docs/transparencypublicsoc.md)

[schoolcontact.externallabs.comaws.cloud@network-operations.net](mailto:schoolcontact.externallabs.comaws.cloud@network-operations.net)
