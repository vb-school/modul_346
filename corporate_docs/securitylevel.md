# Antliuma Aerospace Laboratories (AAL) - Allocation of security permissions and levels directive [directive #AALD-IAP-01]

### 1.1 Zweck dieses Dokuments
Diese Weisung definiert die Vergabe von sogenannten Sicherheitsfreigaben.

### 1.2 Geltungsbereich
Diese Weisung gilt für sämtliche interne sowie externe Mitarbeitende von AAL oder von Unternehmen, welche durch AAL mit der Verarbeitung von bestimmten oder unbestimmten Daten von AAL beauftragt werden respektive wurden. Diese Weisung ist verbindlich. Die Einhaltung dieser Weisung wird durch die Compliance-Abteilung von AAL überwacht, zur Erfüllung dieser Aufgabe kann AAL auch externe Auftragnehmende in diese Prozesse einbinden.

### 1.3 Weisungsverantwortliche Stelle
Das durch AAL beauftragte Unternehmen "Enathrax Laboratories" ist für die Kontrolle der Umsetzung dieser Weisung verantwortlich und arbeitet eng mit der für Compliance verantwortlichen Abteilung von AAL zusammen.

### 2.1 Sicherheitsfreigaben
Sicherheitsfreigaben werden durch die IT-Abteilung von AAL oder durch eine entsprechende Abteilung von Enathrax Laboratories vergeben. Die Sicherheitsfreigaben definieren, inwiefern eine bestimmte Person Zugriff auf Daten einer bestimmter Klassifizierungsstufe erhält.

Die Vergabe der Sicherheitsfreigaben wird dokumentiert und schriftlich festgehalten. Welche Sicherheitsfreigaben Zugriff auf Daten welcher Klassifizierungsstufen gewähren, ist in der [Weisung #AALD-DC-01](/corporate_docs/datenklassifizierung.md) definiert.

Die Sicherheitsfreigaben werden nach dem "Need-to-know"-Prinzip vergeben. 
Die Sicherheitsfreigabe TS5 darf nur an Personen vergeben werden, bei welchen die Integrität sichergestellt ist, keine Vorstrafen vorliegen und keine Auffälligkeiten im Umgang mit Daten von AAL in der Vergangenheit zu verzeichnen sind.



