# Antliuma Aerospace Laboratories (AAL) - General

Die (fiktive) Firma "Antliuma Aerospace Laboratories", welche auch "AAL" genannt wird, wird im Verlauf des Moduls 346 (sowie Modul 143) von Valentin Binotto, im Rahmen dieses Projekts Mitarbeitender des fiktiven IT-Unternehmens Enathrax Laboratories, für seine Arbeiten in der Cloud-Umgebung von Amazon Web Services "AWS" verwendet.

### Benennung von AWS-Ressourcen
Soweit möglich wird zur Benennung von Ressourcen in der AWS-Cloud ein Name in einem der folgendem Formate verwendet:
- ```birdcloud.aerospacelabs.antliuma.com```
- ```birdcloud-aerospacelabs-antliuma-com```
- ```*.birdcloud.aerospacelabs.antliuma.com```
- ```*-birdcloud-aerospacelabs-antliuma-com```

Wobei das ```*```-Zeichen als Wildcard zu verstehen ist. Sämtliche Namen von Ressourcen in der AWS-Cloud bauen also wenn möglich auf der Subdomain ```birdcloud.aerospacelabs.antliuma.com.``` der Domain ```antliuma.com.``` auf. ```antliuma.com.``` befindet sich aktuell im Besitz von Valentin Binotto und die Benennung der AWS-Ressourcen dient somit dazu, zu bestätigen, dass Screenshots, Logs etc. Produkt von Valentin Binottos Arbeiten sind. 

Zur Verifikation des Besitzes der Domain ```antliuma.com.``` und somit auch sämtlicher Subdomains wird vorzugsweise der entsprechende TXT-Eintrag ausgewertet:


```aerospacelabs.antliuma.com.   IN      TXT      "Domain-Owner for Module 346 at TBZ: Binotto (29.08.2023)"```

### Tätigkeit & Ziele des Unternehmens
AAL ist ein fiktives Unternehmen, welches in der Luft- und Raumfahrttechnik tätig ist. Welche Arbeiten AAL in Detail ausführt, ist für mein Projekt in der AWS-Cloud nicht von Relevanz. Wichtig ist jedoch zu erwähnen, dass AAL durch die Tätigkeit im Raumfahrtsektor auch sehr sensitive Daten verarbeitet, welche nicht in die Hände von Regierungsbehörden oder Drittunternehmen gelangen dürfen. Somit existieren bei AAL eine grosse Menge an Sicherheitsvorgaben.

AAL möchte einen Grossteil ihrer derzeit "On-Premises" IT-Infrastruktur in die AWS-Cloud migrieren. Diese Migration erfolgt nicht hauptsächlich aus Kostengründen, sondern aus Überlegungen hinsichtlich des Erhalts von wichtigen Daten. So überlegt sich AAL ob für bestimmte Daten die Replikation über mehrere Regionen eingeführt werden soll, um auch im Falle von grossen Naturkatastrophen bestimmte Daten vor vollständigem Verlust zu schützen. Diese Überlegungen sind jedoch noch nicht definitv abgeschlossen und sind Gegenstand von Gesprächen der Compliance-Verantwortlichen von AAL.

AAL hat neben den öffentlich-zugänglichen Unternehmenszielen, auch interne Ziele, nach welchen gehandelt werden soll. Diese internen Unternehmensziele sind im Nachfolgenden aufgeführt:

> **Respekt**
> - AAL behandelt alle Lebewesen würdevoll und achtet auf die Einhaltung von Menschenrechten. Insbesondere achtet AAL auf den verantwortungsvollen Umgang mit personenbezogenen Daten und schützt die Privatsphäre Rechte aller Beteiligten und Unbeteiligten.
> 
> **Einfachheit**
> - AAL strebt an, die eigene Infrastruktur, insbesondere IT-Infrastruktur, flexibler zu gestalten und so den Mitarbeitenden einfacheren Zugang zu Ressourcen zu ermöglichen. IT-Ressourcen sollen schnell und einfach bereitgestellt werden können, um so die Produktivität der Mitarbeitenden zu fördern.
> 
> **Gesetzestreue**
> - AAL hält sich an sämtliche lokale sowie internationale Gesetzgebungen. Wo die lokale Gesetzgebungen mit Menschenrechten oder internationalen Abkommen in einer Weise kollidieren, welche AAL als nicht mehr vertretbar erachtet, zieht sich AAL aus diesem Markt zurück.
> 
> **Vertrauen und Kontrolle**
> - AAL vertraut seinen Mitarbeitenden, dass diese nach bestem Wissen und Gewissen handeln. Um sichzustellen, dass Menschenrechte, interne Vorgaben sowie gesetzliche Anforderungen erfüllt werden, existiert eine Compliance-Abteilung. Externe Unternehmen, Personen sowie andere interne Abteilungen und Personen können in sämtlichen Compliance-Anliegen durch die Compliance-Abteilung beigezogen werden.
> 

### Bestehende IT-Infrastruktur & Überlegungen zur Teilmigration in die AWS-Cloud
#### Bestehende IT-Infrastruktur
Aktuell werden sämtliche Daten von AAL in ihrem eigenen Datencenter am Standort Winterthur (Zurich, Switzerland) abgespeichert. Sämtliche IT-Ressourcen wie Server, Container, Router, Switches etc. befinden sich ebenfalls in diesem Datencenter. 

Dieses Datencenter (Name: ```AAL-BKNET-DC-ZHWI1```) wird verwaltet und bewirtschaftet durch AAL selbst. Diese "On-Premises" IT-Infrastruktur bietet den Vorteil, dass sämtliche Daten ausschliesslich AAL-Personal zugänglich sind und diese Sicherheit vollkommen transparent für die IT-Abteilung von AAL ist. Die Datenhaltung kann somit garantiert werden. Im AAL-BKNET-DC-ZHWI1 dürfen sämtliche Daten von AAL, aller Klassifizierungs- respektive Geheimhaltungsstufen, abgespeichert und verarbeitet werden. Ein Backup sämtlicher Daten, welche im Datencenter abgespeichert werden, wird jede Nacht um 23:30 manuell angefertigt und die Daten auf externen Datenträgern verschlüsselt abgespeichert und diese Datenträger an einem Standort der IT-Abteilung von AAL gelagert.

Die On-Premises Lösung bietet aber auch den Nachteil der mangelnden Flexibilität: Grosse Rechenleistung kann zwar zur Verfügung gestellt werden, dies muss jedoch je nach Anforderungen manuell durch die IT-Abteilung von AAL erfolgen. Auch ist die Geschwindigkeit von Webinhalten (wie die Webseite von AAL unter [www.aerospacelabs.antliuma.com](https://www.aerospacelabs.antliuma.com) begrenzt, da im Falle eines Websiteaufrufs in den USA, die Webinhalte zuerst über den halben Erdball reisen müssen. Weiter bestht auch das Risiko einer grossen Produktivitätseinschränkung im Falle eines physischen Ereignisses im AAL-BKNET-DC-ZHWI1. So dürfte nach einer physischen Beschädigung des Datencenters, beispielsweise durch eine Naturkatastrophe, die Verfügbarkeit der Daten von AAL eingeschränkt sein, bis die Wiederherstellung der Daten abgeschlossen ist.

Die gesamte On-Premises Lösung von AAL trägt den Namen "autonomous Antliuma private Cloud" ("aapCloud").
Valentin Binotto (Enathrax Laboratories) dient als Berater und externer Experte für die IT-Abteilung von AAL und ist somit auch in die organisatorische sowie technische Bewirtschaftung der gesamten IT-Infrastruktur von AAL, inkl. der On-Premises Umgebung, involviert.

#### Überlegungen zur Teilmigration in die AWS-Cloud
Um getreu dem internen Unternehmensziel "Einfachheit" das zügige Bereitstellen von IT-Infrastruktur zu vereinfachen, hat sich die IT-Abteilung von AAL entschieden, eine Teilmigration von Services in die "Public Cloud" von AWS vorzunehmen. Die dadurch entstehende Cloud-Umgebung in der AWS-Cloud wird von AAL "better inflecting redundant dynamic Cloud" ("birdCloud") genannt.

AAL hat Valentin Binotto (Enathrax Laboratories) mit der Ausarbeitung eines Konzeptes für die technische, organisatorische sowie rechtliche Umsetzung des Projekts "birdCloud" beauftragt. Binotto soll die Umsetzung von "birdCloud" mittels praktischer Umsetzung in von ihm gewählten Cloud-Umgebungen demonstrieren und so Chancen aber auch Risiken der Nutzung der AWS-Cloud aufzeigen. 

AAL verarbeitet hochsensitiven Daten (z.B. Informationen zur Steuerung von Satelliten, Baupläne von Satelliten), mittelsensitive Daten (z.B. Details zur internen IT-Infrastruktur oder dem organisatorischen Aufbau von AAL) sowie nicht-sensitive Daten (z.B. Messdaten von Satelliten oder von diesen angefertigte Fotographien). Zur Einstufung der Daten existiert ein Klassifizierungssystem, welches verbindlich auf alle Daten von AAL angewendet werden muss. Anhand dieser Klassifizierung einer Datei entscheidet sich dann, ob diese in der "birdCloud" verarbeitet und/oder gespeichert werden darf. Entsprechende Angaben zu den verschiedenen Klassifizierungsstufen finden sich in der [entsprechenden Weisung (#AALD-DC-01)](/corporate_docs/datenklassifizierung.md).

Die Daten, welche gemäss Klassifizierungssystem in der "birdCloud" abgelegt werden dürfen, werden je nach Wiederherstellungsschutzgrad, welche ebenfalls im oben verlinkten Dokument zu den Klassifizierungsstufen definiert werden, in der "birdCloud" entsprechend vor Datenverlust gesichert. Dies kann beispielsweise über eine Replikation über mehrere AWS-Regions erfolgen.
