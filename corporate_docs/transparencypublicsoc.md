# Antliuma Aerospace Laboratories (AAL) - Transparency and openness towards the public directive [directive #AALD-TPA-01]

### 1.1 Zweck dieses Dokuments
Diese Weisung definiert den Umgang mit Daten von AAL, welche der Öffentlichkeit zugänglich gemacht werden oder zugänglich gemacht werden sollen.

### 1.2 Geltungsbereich
Diese Weisung gilt für sämtliche interne sowie externe Mitarbeitende von AAL oder von Unternehmen, welche durch AAL mit der Verarbeitung von bestimmten oder unbestimmten Daten von AAL beauftragt werden respektive wurden. Diese Weisung ist verbindlich. Die Einhaltung dieser Weisung wird durch die Compliance-Abteilung von AAL überwacht, zur Erfüllung dieser Aufgabe kann AAL auch externe Auftragnehmende in diese Prozesse einbinden.

### 1.3 Weisungsverantwortliche Stelle
Das durch AAL beauftragte Unternehmen "Enathrax Laboratories" ist für die Kontrolle der Umsetzung dieser Weisung verantwortlich und arbeitet eng mit der für Compliance verantwortlichen Abteilung von AAL zusammen.

### 2.1 Sinn und Zweck von Transparenz und Offenheit gegenüber der Öffentlichkeit
AAL ist der Überzeugung, dass die Arbeiten von Unternehmen und Organisationen jeglicher Art dem Wohle Aller dienen sollen. AAL handelt nach der Überzeugung "Work to live, don't live to work" und möchte durch die Bemühungen, möglichst viele Daten, welche AAL bei der Erforschung des Himmels und des Weltraumes gewinnt, der Öffentlichkeit zugänglich zu machen, der Bevölkerung so versichern, dass die Arbeit von AAL nicht der Profitmaximierung dient.

Weiter ist AAL davon überzeugt, dass die Veröffentlichung von Forschungsdaten der wissenschaftlichen Community eine gewisse Unterstützung bei der Erforschung von Weltraum und Erde bietet und so die gesamte Forschung auf diesem Gebiet voranbringt.

### 2.2 Veröffentlichung von Daten
Gemäss der Weisung zu Datenklassifizierung ([directive #AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md)) werden sämtliche Daten, welche AAL verarbeitet in entsprechende Klassifizierungsstufen eingeteilt. Diese Klassifizierungsstufen legen unter anderem auch fest, ob die eingestuften Daten beispielsweise der Öffentlichkeit zugänglich gemacht werden.

Die Vorgehensweise zur Einstufung von Daten in Klassifizierungsstufen ist der [directive #AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md) zu entnehmen.

Sollen Daten veröffentlicht werden, ist zuerst die Klassifizierungsstufe des fraglichen Datensatzes zu prüfen. Lässt diese eine Veröffentlichung zu, darf mit folgenden Schritten fortgefahren werden:

- Informationsmail an die Kommunikationsabteilung von AAL (communications@aerospacelabs.antliuma.com) (inkl. URLs auf die fraglichen Daten)
- Die Kommunikationsabteilung von AAL prüft die Daten auf deren Klassifizierungsstufen
- Die Kommunikationsabteilung von AAL macht die Daten öffentlich zugänglich und stellt dem ursprünglichen Absender der Mail von Schritt 1 die URLs auf die öffentlichen Daten zur Verfügung



