# Antliuma Aerospace Laboratories (AAL) - Processing of third party data directive [directive #AALD-PID-01]

### 1.1 Zweck dieses Dokuments
Diese Weisung definiert den Umgang mit Daten, mit deren Verarbeitung oder Speicherung AAL durch Drittparteien beauftragt wurde.

### 1.2 Geltungsbereich
Diese Weisung gilt für sämtliche interne sowie externe Mitarbeitende von AAL oder von Unternehmen, welche durch AAL mit der Verarbeitung von bestimmten oder unbestimmten Daten von AAL beauftragt werden respektive wurden. Diese Weisung ist verbindlich. Die Einhaltung dieser Weisung wird durch die Compliance-Abteilung von AAL überwacht, zur Erfüllung dieser Aufgabe kann AAL auch externe Auftragnehmende in diese Prozesse einbinden.

### 1.3 Weisungsverantwortliche Stelle
Das durch AAL beauftragte Unternehmen "Enathrax Laboratories" ist für die Kontrolle der Umsetzung dieser Weisung verantwortlich und arbeitet eng mit der für Compliance verantwortlichen Abteilung von AAL zusammen.

### 2.1 Daten von Drittparteien
Daten, mit deren Speicherung, Bearbeitung oder sonstigen Verarbeitung AAL durch eine juristische oder natürliche dritte Person beauftragt wurde, gelten als "third party data" oder "Drittparteiendaten".

### 2.2 Notwendigkeit dieser Weisung
Da AAL mit verschiedensten Drittparteien Vertragsbeziehungen eingegangen ist oder eingehen wird und AAL in Folge dessen verschiedenste Vertragsbedingungen zum Umgang mit Drittparteiendaten beachten muss, hat AAL sich entschlossen, sämtliche Drittparteiendaten gemäss einer einheitlichen Weisung zu verarbeiten, welche sämtliche Vertragsbedingungen abdeckt, indem sie sich an den striktesten Vertragsbedingungen orientiert.

Diese einheitliche Weisung verhindert Unklarheiten und Missverständnisse im Umgang mit Drittparteiendaten und gewährleistet eine höhere Datensicherheit. Auch vereinfacht diese einheitliche Weisung die Arbeit der Mitarbeitenden von AAL erheblich.

### 2.3 Umgang mit Daten von Drittparteien
Drittparteiendaten sind als äusserst sensitiv anzusehen und werden einheitlich immer der Klassifizierungsstufe ``E3 - CONFIDENTIAL`` zugewiesen. 
Im Umgang mit sämtlichen Daten ist zwingend die Weisung [#AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md) zu beachten.

Drittparteiendaten dürfen nur in Absprache mit der entsprechenden Drittpartei sowie in Absprache mit Enathrax Laboratories einer tieferen Klassifizierungsstufe zugewiesen werden.

AAL sammelt keine Personen- oder Drittparteiendaten mit dem Ziel, diese weiterzuverkaufen. Das Anlegen von Datensammlungen über Dritte soll auf ein Minimum beschränkt werden und nur erfolgen soweit für die Erfüllung von vertraglichen, gesetzlichen oder internen Weisungen erforderlich.

