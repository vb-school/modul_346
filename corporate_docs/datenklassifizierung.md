# Antliuma Aerospace Laboratories (AAL) - Data classification directive [directive #AALD-DC-01]

### 1.1 Zweck dieses Dokuments
Dieses Dokument regelt den Umgang mit sämtlichen Daten, welche durch Antliuma Aerospace Laboratories, im Nachfolgenden "AAL", verarbeitet und/oder gespeichert werden. Durch die Einstufung sämtlicher Daten von AAL in eine Klassifizierungsstufe soll sichergestellt werden, dass sensitive Daten nicht von Unbefugten eingesehen werden können und somit keine Schäden in dieser Hinsicht für AAL entstehen.

### 1.2 Geltungsbereich
Diese Weisung gilt für sämtliche interne sowie externe Mitarbeitende von AAL oder von Unternehmen, welche durch AAL mit der Verarbeitung von bestimmten oder unbestimmten Daten von AAL beauftragt werden respektive wurden. Diese Weisung ist verbindlich und ist somit zwingend bei sämtlichen Daten von AAL anzuwenden. Die Einhaltung dieser Weisung wird durch die Compliance-Abteilung von AAL überwacht, zur Erfüllung dieser Aufgabe kann AAL auch externe Auftragnehmende in diese Prozesse einbinden.

### 1.3 Weisungsverantwortliche Stelle
Das durch AAL beauftragte Unternehmen "Enathrax Laboratories" ist für die Kontrolle der Umsetzung dieser Weisung verantwortlich und arbeitet eng mit der für Compliance verantwortlichen Abteilung von AAL zusammen.

### 2.1 Klassifizierungsstufen
Sämtliche Daten, welche von AAL generiert, gespeichert oder andersweitig verarbeitet werden, müssen zwingend in eine der folgenden Klassifizierungsstufen eingestuft werden:

**E1 - PUBLIC**
- Daten dieser Stufe gelten als öffentlich zugänglich. Jegliche Drittpersonen oder andere Drittparteien dürfen auf Daten dieser Stufe unbeschränkt zugreifen.
- Daten dieser Stufe dürfen auf unverschlüsselten Datenträgern, nicht-verwalteten Cloud-Umgebungen, sowie auf Webseiten oder Plattformen jeglich anderer Art abgelegt werden.
- Bei der Ablage in einer private oder public Cloud-Umgebung von AAL gelten keine Einschränkungen bzgl. dem geografischen Standort der Datencenter.

**E2 - INTERNAL**
- Daten dieser Stufe gelten als intern und sind sämtlichen internen und externen Mitarbeitenden zugänglich. Auf Daten dieser Stufe darf auch in Daten der Stufe E1 verwiesen werden, jedoch dürfen keine als E2 eingestuften Inhalte in Daten der Stufe E1 vorhanden sein.
- Daten dieser Stufe dürfen auf den verwalteten private sowie public Cloud-Umgebungen von AAL abgelegt werden. Namentlich in der "birdCloud" sowie der "aapCloud". Die Daten müssen innerhalb der private oder public Cloud-Umgebungen von AAL nicht separat verschlüsselt werden. Daten dieser Stufe dürfen über die verwalteten Kommunikationsplattformen von AAL geteilt werden.
- Bei der Ablage in einer private oder public Cloud-Umgebung von AAL gelten keine Einschränkungen bzgl. dem geografischen Standort der Datencenter.

**E3 - CONFIDENTIAL**
- Daten dieser Stufe sind sämtlichen internen Mitarbeitenden zugänglich. Auf Daten dieser Stufe darf auch in Daten der Stufe E2 verwiesen werden, jedoch dürfen keine als E3 eingestuften Inhalte in Daten der Stufe E2 vorhanden sein.
- Daten dieser Stufe dürfen auf den verwalteten private sowie public Cloud-Umgebungen von AAL abgelegt werden. Namentlich in der "birdCloud" sowie der "aapCloud". Die Daten müssen innerhalb der private oder public Cloud-Umgebungen von AAL nicht separat verschlüsselt werden. Daten dieser Stufe dürfen über die verwalteten Kommunikationsplattformen von AAL geteilt werden.
- Bei der Ablage in einer private oder public Cloud-Umgebung von AAL muss der geografische Standort des Datencenters, in welchem die Daten abgespeichert oder verarbeitet werden sollen, sich zwingend innerhalb des europäischen Raumes befinden. Keinesfalls dürfen Daten dieser Stufe im US-Amerikanischen Raum gespeichert oder verarbeitet werden. 

**E4 - SECRET**
- Daten dieser Stufe sind lediglich Mitarbeitenden mit der Sicherheitsfreigabe OS4 zugänglich.
- Daten dieser Stufe dürfen lediglich auf der private Cloud-Umgebung (aapCloud) von AAL abgelegt werden. Die Daten müssen innerhalb der private Cloud-Umgebung von AAL nicht separat verschlüsselt werden.
- Bei der Ablage in der private Cloud-Umgebung von AAL (aapCloud) muss zwingend als Ablageort der Daten das Datencenter "AAL-BKNET-DC-ZHWI1" angegeben werden.
- Auf Daten dieser Stufe darf auch in Daten der Stufe E3 verwiesen werden, jedoch dürfen keine als E4 eingestuften Inhalte in Daten der Stufe E3 vorhanden sein.

**E5 - TOP SECRET**
- Daten dieser Stufe sind lediglich Personen mit der Sicherheitsfreigabe TS5 zugänglich.
- Daten dieser Stufe dürfen lediglich auf der private Cloud-Umgebung (aapCloud) von AAL abgelegt werden. Sämtliche Daten dieser Stufe müssen vor dem Upload in die private Cloud von AAL (aapCloud) mittels AES-256 Verschlüsselung verschlüsselt werden. Das entsprechende Passwort muss mindestens 20 Zeichen aufweisen. Das entsprechende Passwort darf nur in den, durch AAL freigegebenen, Passwort-Verwaltungsprogrammen abgelegt werden.
- Bei der Ablage in der private Cloud-Umgebung von AAL (aapCloud) muss zwingend als Ablageort der Daten das Datencenter "AAL-BKNET-DC-ZHWI1" angegeben werden.
- Es darf in Daten der Stufen E1, E2, E3 sowie E4 nicht auf Daten der Stufe E5 verwiesen werden.

### 2.2 Hierarchisches Prinzip
- Daten der Klassifizierungsstufen E1, E2, E3, E4 dürfen in Daten der Klassifizierungsstufe E5 vorhanden sein.
- Daten der Klassifizierungsstufen E1, E2, E3 dürfen in Daten der Klassifizierungsstufe E4, E5 vorhanden sein.
- Daten der Klassifizierungsstufen E1, E2 dürfen in Daten der Klassifizierungsstufe E3, E4, E5 vorhanden sein.
- Daten der Klassifizierungsstufen E1 dürfen in Daten der Klassifizierungsstufe E2, E3, E4, E5 vorhanden sein.

Folglich haben Inhaber:innen der Sicherheitsfreigabe TS5 Zugriff auf sämtliche Daten von AAL (über sämtliche Klassifizierungsstufen hinweg).
Inhaber:innen der Sicherheitsfreigabe OS4 haben Zugriff auf Daten der Klassifizierungsstufen E1, E2, E3, E4.

### 3.1 Wiederherstellungsschutzgrad
Sämtliche Daten von AAL erhalten neben der Klassifizierungsstufe einen Wiederherstellungsschutzgrad, welcher bezeichnet, inwiefern ein Datenobjekt vor Verlust oder Beschädigung geschützt werden soll.
Je nach Wiederherstellungsschutzgrad werden andere Massnahmen getroffen, um sicherzustellen, dass die im Wiederherstellungsschutzgrad geforderte Garantie der Datenverfügbarkeit gewährleistet werden kann. Im Folgenden werden die verschiedenen Ausprägungen des Wiederherstellungsschutzgrads aufgeführt:

**G1 - NO BACKUP**
- Daten mit diesem Grad werden nicht mittels einem Backup gesichert. Der Verlust dieser Daten ist vertretbar und eine Datensicherung würde einen unverhältnismässigen Aufwand darstellen.

**G2 - SIMPLE BACKUP**
- Daten mit diesem Grad werden mittels einem einfachen Backup gesichert. Folglich wird eine Kopie dieses Datenobjekts angefertigt und in der selben Umgebung abgelegt. Dieses Backup dient lediglich der Wiederherstellung im Falle einer versehentlichen Löschung oder eines Ausfalls eines Datenträgers oder einer bestimmten Cloud-Ressource.

**G3 - DISASTER RESISTANT BACKUP**
- Daten mit diesem Grad sollen auch im Falle eines weitreichenden Ereignisses noch wiederherstellbar sein. Diese Daten werden mehrfach repliziert und die Kopien in unterschiedlichen Ablageorten abgelegt.


Das Anfertigen von Sicherungskopien (Backups) soll jederzeit im Einklang mit der entsprechenden Klassifizierungsstufe, welche dem Datenobjekt zugewiesen wurde, stehen. Ist dies nicht möglich, hat die Klassifizierungsstufe Vorrang.

### 4.1 Schreibweise von Klassifizierungsstufe & Wiederherstellungsschutzgrad
Daten werden mit der zugewiesenen Klassifizierungsstufe sowie dem Wiederherstellungsschutzgrad beschriftet. Dies erfolgt mittels Meta-Daten und wenn möglich mit Beschriftungen innerhalb des Datenobjekts. 

Diese Beschriftungen haben zwingend in einem der folgend aufgeführten Formate zu erfolgen:
- ```[Kürzel Klassifizierungsstufe]-[Kürzel Wiederherstellungsschutzgrad]```
- ```[Kürzel Klassifizierungsstufe]-[Kürzel Wiederherstellungsschutzgrad] // [Klassifizierungsstufe] - [Wiederherstellungsschutzgrad]```

Folgend entsprechende Beispiele:
- ```E1-G1```
- ```E1-G1 // PUBLIC - NO BACKUP```
### 4.2 Einstufung von Daten in Klassifizierungsstufe & Wiederherstellungsschutzgrad
AAL geht davon aus, dass sämtliche Mitarbeitende eigenständig und verantwortungsvoll beurteilen können, welche Klassifizierungsstufe und welcher Wiederherstellungsschutzgrad einem bestimmten Datenobjekt zugeordnet werden sollen. Dabei ist zu beachten, dass Daten im Zweifel besser höher, also als sensitiver respektive relevanter hinsichtlich Verfügbarkeit, eingestuft werden.

Bei Unklarheiten können sich Mitarbeitende jederzeit an die Compliance-Abteilung von AAL wenden.
Einstufungen der Compliance-Abteilung von AAL oder von Personen der Sicherheitsfreigabe TS5 sind definitiv und bestimmen die definitive Klassifizierungsstufe und den Wiederherstellungsschutzgrad der Daten.

Ist ein Datenobjekt einer Klassifizierungsstufe oder/und einem Wiederherstellungsschutzgrad zugeordnet, bedarf es der Zustimmung der Compliance-Abteilung von AAL oder der Zustimmung einer Person im Besitze der Sicherheitsfreigabe TS5, um die Klassifizierungsstufe oder/und den Wiederherstellungsschutzgrad des Datenobjekts zu ändern.

### 5.1 Merkmale zur Einstufung von Daten
Wie bereits in Abschnitt 4.2 dieser Weisung erläutert, geht AAL davon aus, dass "sämtliche Mitarbeitende eigenständig und verantwortungsvoll beurteilen können, welche Klassifizierungsstufe und welcher Wiederherstellungsschutzgrad einem bestimmten Datenobjekt zugeordnet werden sollen.".
Um die Mitarbeitenden dennoch bei dieser Einstufung zu unterstützen finden sich im Nachfolgenden einige Merkmale welche dazu dienen können um die/den niedrigste/n zu wählende/n Klassifizierungsstufe/Wiederherstellungsschutzgrad zu identifizieren. Diese Auflistung stellt keineswegs eine abschliessende oder verbindliche Liste zur Dateneinstufung dar.

- Personenbezogene Daten (z.B. Namen, Geburtsdatum, Adresse, Interessen, Gesundheitsangaben etc.) sind mindestens als E3-G1 einzustufen.
- Messdaten von Satelliten (z.B. Messdaten von AAL-Satelliten oder von diesen angefertigte Fotographien) sind mindestens als E1-G2 einzustufen.
- Details AAL (z.B. Details zur internen IT-Infrastruktur oder dem organisatorischen Aufbau von AAL) sind mindestens als E2-G2 einzustufen.
- Wichtige Daten zur Bedienung von Satelliten (z.B. Informationen zur Steuerung von Satelliten, Baupläne von Satelliten) sind mindestens als E5-G3 einzustufen.
