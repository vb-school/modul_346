# Antliuma Aerospace Laboratories (AAL) - Project "birdCloud"

## Einleitung & Namenskonvention
### Antliuma Aerospace Laboratories (AAL)
Die (fiktive) Firma "Antliuma Aerospace Laboratories", welche auch "AAL" genannt wird, wird im Verlauf des Moduls 346 (sowie Modul 143) von Valentin Binotto, im Rahmen dieses Projekts Gründer des fiktiven IT-Unternehmens Enathrax Laboratories, für seine Arbeiten in der Cloud-Umgebung von Amazon Web Services "AWS" verwendet. Jegliche Angaben zum fiktiven Unternehmen "Antliuma Aerospace Laboratories" sind frei erfunden und dienen ausschlisslich der Verwendung im Rahmen der Schulmodule 346 und 143.

#### Tätigkeit des Unternehmens
AAL ist ein fiktives Unternehmen, mit Sitz in Winterthur (Schweiz), welches in der Luft- und Raumfahrttechnik tätig ist. Welche Arbeiten AAL in Detail ausführt, ist für mein Projekt in der AWS-Cloud nicht von Relevanz. Wichtig ist jedoch zu erwähnen, dass AAL durch die Tätigkeit im Raumfahrtsektor auch sehr sensitive Daten verarbeitet, welche nicht in die Hände von Regierungsbehörden oder Drittunternehmen gelangen dürfen. Somit existieren bei AAL eine grosse Menge an Sicherheitsvorgaben.

#### Mitarbeitende von AAL
AAL besitzt 500 Mitarbeitende, welche hauptsächlich am Firmenhauptsitz in Winterthur tätig sind. Teilweise sind Mitarbeitende auch ausserhalb von Winterthur tätig bzw. sind zwecks Geschäftsreisen ausser Haus. 

### Namenskonvention
Soweit möglich wird zur Benennung von Ressourcen in der AWS-Cloud ein Name in einem der folgendem Formate verwendet:
- ```birdcloud.aerospacelabs.antliuma.com```
- ```birdcloud-aerospacelabs-antliuma-com```
- ```*.birdcloud.aerospacelabs.antliuma.com```
- ```*-birdcloud-aerospacelabs-antliuma-com```
- ```birdcloud.aal.antliuma.com```
- ```birdcloud-aal-antliuma-com```
- ```*.birdcloud.aal.antliuma.com```
- ```*.birdcloud-aal-antliuma-com```

Wobei das ```*```-Zeichen als Wildcard zu verstehen ist. Sämtliche Namen von Ressourcen in der AWS-Cloud bauen also wenn möglich auf der Subdomain ```birdcloud.aerospacelabs.antliuma.com.``` der Domain ```antliuma.com.``` auf. ```antliuma.com.``` befindet sich aktuell im Besitz von Valentin Binotto und die Benennung der AWS-Ressourcen dient somit dazu, zu bestätigen, dass Screenshots, Logs etc. Produkt von Valentin Binottos Arbeiten sind. 

Zur Verifikation des Besitzes der Domain ```antliuma.com.``` und somit auch sämtlicher Subdomains wird vorzugsweise der entsprechende TXT-Eintrag ausgewertet:


```aerospacelabs.antliuma.com.   IN      TXT      "Domain-Owner for Module 346 at TBZ: Binotto (29.08.2023)"```

Weitere Domains, welche zwar Verwendung im Laufe dieses Projektes finden aber nicht der Domain ```antliuma.com.``` hierarchisch untergeordnet sind, werden via TXT-Eintrag unter der Domain ```vroot.aerospacelabs.antliuma.com.```, wie im Internet Draft ["draft-valentinbinotto-verifyrootdomain-00"](https://datatracker.ietf.org/doc/draft-valentinbinotto-verifyrootdomain/) im Detail beschrieben, verifiziert.

- "Antliuma Aerospace Laboratories" wird im Verlaufe dieses Projekts und in Unterlagen in diesem GIT-Repository auch bezeichnet/abgekürzt als "AAL".
- "Enathrax Laboratories" (Valentin Binotto) wird im Verlaufe dieses Projekts und in Unterlagen in diesem GIT-Repository auch bezeichnet/abgekürzt als "EnatLabs", "Wir" oder "Binotto".

## Rahmenbedingungen
### Unternehmenswerte

AAL hat öffentlich-zugängliche Unternehmenswerte definiert (welche auch als Unternehmensziele verstanden werden dürfen), nach welchen gehandelt werden soll. Diese Werte sind im Nachfolgenden aufgeführt:

> **Respekt**
> - AAL behandelt alle Lebewesen würdevoll und achtet auf die Einhaltung von Menschenrechten. Insbesondere achtet AAL auf den verantwortungsvollen Umgang mit personenbezogenen Daten und schützt die Privatsphäre Rechte aller Beteiligten und Unbeteiligten. AAL respektiert aber auch das Interesse der Öffentlichkeit an Daten und stellt deshalb im Rahmen einer entsprechenden Weisung auch AAL-Material der Öffentlichkeit zur Verfügung.
> 
> **Einfachheit**
> - AAL strebt an, die eigene Infrastruktur, insbesondere IT-Infrastruktur, flexibler zu gestalten und so den Mitarbeitenden einfacheren Zugang zu Ressourcen zu ermöglichen. IT-Ressourcen sollen schnell und einfach bereitgestellt werden können, um so die Produktivität der Mitarbeitenden zu fördern.
> 
> **Gesetzestreue**
> - AAL hält sich an sämtliche lokale sowie internationale Gesetzgebungen. Wo die lokale Gesetzgebungen mit Menschenrechten oder internationalen Abkommen in einer Weise kollidieren, welche AAL als nicht mehr vertretbar erachtet, zieht sich AAL aus diesem Markt zurück.
> 
> **Vertrauen und Kontrolle**
> - AAL vertraut seinen Mitarbeitenden, dass diese nach bestem Wissen und Gewissen handeln. Um sichzustellen, dass Menschenrechte, interne Vorgaben sowie gesetzliche Anforderungen erfüllt werden, existiert eine Compliance-Abteilung. Externe Unternehmen, Personen sowie andere interne Abteilungen und Personen können in sämtlichen Compliance-Anliegen durch die Compliance-Abteilung beigezogen werden. AAL kontrolliert, trotz dem grossen Vertrauen in seine Mitarbeitenden und Partner, dass sämtliche Weisungen von AAL, Vertragsbedingungen und rechtliche Anforderungen von AAL Mitarbeitenden und Partnern eingehalten werden.
> 

### Interne Vorschriften & Weisungen
Zum Schutze des geistigen Eigentums, der physischen Sicherheit sowie der verarbeiteten Daten, existieren bei AAL einige interne Weisungen und Vorgaben, nach welchen zwingend gehandelt werden soll. Die Mitarbeitenden sind angewiesen, diese Vorgaben strikte umzusetzen und so zum Schutze von AAL beizutragen.

Nachfolgend die Auflistung der entsprechenden internen Vorschriften und Weisungen:

- [Weisung #AALD-DC-01 (Datenklassifizierung)](/corporate_docs/datenklassifizierung.md)
- [Weisung #AALD-PID-01 (Verarbeitung von Daten von Drittparteien)](/corporate_docs/thirddata.md)
- [Weisung #AALD-IAP-01 (Vergabe von Sicherheitsfreigaben)](/corporate_docs/securitylevel.md)
- [Weisung #AALD-TPA-01 (Transparenz gegenüber der Öffentlichkeit)](/corporate_docs/transparencypublicsoc.md)

### Gesetzliche & Vertragliche Vorschriften & Weisungen
Durch die Zusammenarbeit mit Drittparteien ist AAL teilweise an zusätzliche vertragliche Abmachungen bezüglich Datenschutz/-sicherheit gebunden und hat nach diesen zu handeln. Weiter ist AAL selbstredend gezwungen, die gesetzlichen Bestimmungen zur Verarbeitung von Daten zu respektieren und entsprechend zu handeln.

AAL sitzt in Winterthur (Schweiz) und somit gelten unter anderem die gesetzlichen Vorgaben der schweizerischen Eidgenossenschaft. Nachfolgend findet sich eine (nicht zwangsläufig abschliessende) Auflistung der somit relevanten gesetzlichen Grundlagen zum Schutz von Daten:

- [National: Bundesgesetz über den Datenschutz](https://www.fedlex.admin.ch/eli/cc/2022/491/de)
- [National: Verordnung über den Datenschutz](https://www.fedlex.admin.ch/eli/cc/2022/568/de)

Um die Einhaltung der teilweise sehr unterschiedlichen vertraglichen Vorgaben bezüglich Datenschutz/-sicherheit zu garantieren, behandelt AAL die Daten von Drittparteien mit äusserster Vorsicht und hat die Anweisungen dazu in der [Weisung #AALD-PID-01](/corporate_docs/thirddata.md) vereinheitlicht. Somit besitzen die genauen Vertragsinhalte für die Mitarbeitenden von AAL keine Relevanz.

## Bestehende IT-Infrastruktur
Aktuell werden sämtliche Daten von AAL in ihrem eigenen Datencenter am Standort Winterthur (Zurich, Switzerland) abgespeichert. Sämtliche IT-Ressourcen wie Server, Container, Router, Switches etc. befinden sich ebenfalls in diesem Datencenter. 

Dieses Datencenter (Name: ```AAL-BKNET-DC-ZHWI1```) wird verwaltet und bewirtschaftet durch AAL selbst. Diese "On-Premises" IT-Infrastruktur bietet den Vorteil, dass sämtliche Daten ausschliesslich AAL-Personal zugänglich sind und die Vertraulichkeit und Sicherheit der Daten somit garantiert ist. Die Speicherung der Daten in schweizer Hoheitsgebiet kann folglich gewährleistet werden. Im AAL-BKNET-DC-ZHWI1 dürfen sämtliche Daten von AAL, aller Klassifizierungs- respektive Geheimhaltungsstufen, abgespeichert und verarbeitet werden. Die Daten desselben Wiederherstellungsschutzgrades werden jeweils in einem virtuellen "Bucket" abgelegt und mittels Metadaten-Tags sowie Kennzeichnung innerhalb des Datensatzes selbst (beispielsweise direkt auf dem PDF-Dokument) wird die Klassifizierungsstufe eines Datensatzes vermerkt.
Eine Sicherungskopie der Daten mit Wiederherstellungsschutzgrad ``G2`` oder ``G3``, welche im Datencenter abgespeichert werden, wird jede Nacht um 23:30 manuell angefertigt und die Daten auf externen Datenträgern verschlüsselt abgespeichert und diese Datenträger an einem Standort der IT-Abteilung von AAL (ebenfalls in Winterthur (Zurich, Switzerland)) gelagert.

Die Webseiten von AAL (z.B. [www.aerospacelabs.antliuma.com](https://www.aerospacelabs.antliuma.com/)) werden ebenfalls in einem entsprechenden Subnetz des Datencenters ```AAL-BKNET-DC-ZHWI1``` gespeichert und entsprechend im öffentlichen Internet bereitgestellt.

Die gesamte On-Premises Lösung von AAL trägt den Namen "autonomous Antliuma private Cloud" ("aapCloud"). Die Domäne ```aapcloud.aerospacelabs.antliuma.com``` ist der besagten Umgebung zugeordnet.

Mitarbeitende von AAL welche sich vorübergehend nicht am Geschäftsstandort in Winterthur sondern zwecks Geschäftsreisen ausser Haus befinden, können während der Dauer dieser "Abwesenheit" nicht auf die Ressourcen der aapCloud zugreifen. Sàmtliche Daten, Dokumente etc. werden durch diese Mitarbeitende lokal auf dem mitgeführten Geschäftsgerät gespeichert und nach Ankunft am Geschäftsstandort in Winterthur manuell in die aapCloud übertragen.
 
Valentin Binotto (Enathrax Laboratories) dient als Berater und externer Experte für die IT-Abteilung von AAL und ist somit auch in die organisatorische sowie technische Bewirtschaftung der gesamten IT-Infrastruktur von AAL, inkl. der On-Premises Umgebung, involviert.

### Darstellung Aufbau & Struktur
Der detailierte Aufbau der aapCloud findet sich in einem [entsprechenden Netzwerkplan](/Weitere_Ressourcen/Referenz_Dokumente/aapCloud_Netzwerkaufbau.pdf):

![Bild aapCloud Aufbau](/Weitere_Ressourcen/Referenz_Dokumente/aapCloud_Netzwerkaufbau_Bild.png)


## Auftrag Abklärungen zur Verwendung einer Cloud-Umgebung
### Auftragsschreiben von AAL an Enathrax Laboratories

    Von: Benjamin Redam, IT-NET-COL <benjamin.redam@aerospacelabs.antliuma.com>
    An: Valentin Binotto, ENATHRAX LABORATORIES - MAA-HIORG <valentin@publicmails.enatlabs.com>
    Gesendet am: 11.08.2023 12:19:35
    Vertraulichkeit: E2-G1 // INTERNAL - NO BACKUP
    #############################################################################

    Geschätzter Herr Binotto,
    Geschätzte Damen und Herren,

    Wir kontaktieren Sie aufgrund einer Entscheidung der Geschäftsleitung von Antliuma Aerospace Laboratories (nachfolgend "AAL") im Zusammenhang mit der aktuellen IT-Infrastruktur von AAL. Die Geschäftsleitung hat die Vorteile des sogenannten "Cloud Computings" erkannt und hat eine entsprechende Abklärung der Optionen für AAL angeordnet. Mit dieser Angelegenheit möchte AAL Sie, beziehungsweise Ihr Unternehmen Enathrax Laboratories, beauftragen. Da Enathrax Laboratories bereits für die bereits vorhandene IT-Infrastruktur von AAL mitverantwortlich ist, ist AAL überzeugt, dass dadurch das benötigte tiefe technische Verständnis der Infrastruktur von AAL gegeben ist.

    AAL beauftragt Enathrax Laboratories mit folgenden Punkten:

    - Analyse und Abklärung der Chancen & Risiken einer Cloud-Lösung für AAL im Vergleich zur bestehenden On-Premises-Lösung
    - Analyse der technischen Anforderungen (wie Speicherplatz, Geschwindigkeiten von Netzwerkverbindungen etc.), welche AAL an eine allfällige Cloud-Lösung hat
    - Analyse der zu erwartenden Kosten einer allfälligen Cloud-Lösung
    - Auswahl eines zu bevorzugenden Anbieters von Cloud Computing Diensten
    - Abklärung inwiefern eine Teil- oder Komplettmigration der gesamten IT-Infrastruktur von AAL in eine Cloud-Umgebung möglich und sinnvoll ist
    - Planung und Definition von Massnahmen um die Einhaltung der Weisung #AALD-DC-01 (zur Datenklassifizierung und Datenerhaltung) garantieren zu können
    - Planung und Definition von Massnahmen um die Einhaltung der Weisungen #AALD-PID-01 sowie #AALD-TPA-01 ebenfalls garantieren zu können

    Falls Enathrax Laboratories zum Schluss kommt, dass eine Teil- oder Komplettmigration in die Cloud machbar wäre, beauftragt AAL Enathrax Laboratories zusätzlich mit folgenden Punkten:
    - Definition des genauen Umfangs, der Architektur sowie Erstellung eines Konzepts für die Cloud-Umgebung
    - Definition welche Daten und Ressourcen in die neu geschaffene Cloud-Umgebung migriert werden sollen
    - Planen eines Datensicherungssystems, um den Erhalt der in der Cloud abgelegten Daten zu garantieren, soweit durch die Weisung #AALD-DC-01 gefordert
    - Praktische Umsetzung der vorgängig definierten Cloud-Architektur innerhalb einer, durch Enathrax Laboratories teilweise oder vollständig kontrollierten und verwalteten, Cloud-Umgebung zu Testzwecken


    AAL erwartet von Enathrax Laboratories eine entsprechende Dokumentation der durchgeführten Arbeiten, der Abklärungen, Analysen etc.
    Auf Basis dieser Dokumentation von Enathrax Laboratories wird AAL dann die definitive Entscheidung fällen und im entsprechenden Falle Enathrax Laboratories mit der finalen Umsetzung des Konzepts beauftragen, welche dann schlussendlich auch, soweit durch das Konzept zugelassen, in der produktiven Umgebung von AAL Verwendung finden wird.

    Aufgrund der Verwaltung der IT-Infrastruktur von AAL, besitzen die relevanten Mitarbeitenden von Enathrax Laboratories die Sicherheitsfreigabe TS5 bereits. Die entsprechenden Personen sind nachfolgend aufgeführt:

    - Valentin Binotto (Enathrax Laboratories) (username: valentin-43-44.network-operations.net)

    Der so zustande gekommene Auftrag für Enathrax Laboratories untersteht den Vertragsbedingungen des bereits existierenden Basisvertrags zwischen AAL und Enathrax Laboratories.
    Bei allfälligen Fragen steht AAL Ihnen gerne unter den folgenden Kontaktdaten zur Verfügung:
    ---------------------------------
    Antliuma Aerospace Laboratories
    Information Technology
    Networks and other Infrastructure
    Antliumastrasse 1
    8400 Winterthur

    it-net@groups.aerospacelabs.antliuma.com
    ---------------------------------

    AAL hat vollstes Vertrauen in Enathrax Laboratories und dankt für die angenehme Zusammenarbeit
    Mit freundlichen Grüsse


    Benjamin Redam
    Head of IT-Development Department
    Antliuma Aerospace Laboratories

    (Im Auftrage der Geschäftsleitung von Antliuma Aerospace Laboratories)




### Definition Projektname
Enathrax Laboratories wurde, wie in der [Mail von AAL an Enathrax Laboratories](#auftragsschreiben-von-aal-an-enathrax-laboratories) ersichtlich, mit der Abklärung, Analyse und Konzepterstellung für eine allfällige Cloud-Umgebung beauftragt. Dieser entsprechende Auftrag und das dadurch entstehende Konzept bzw. die Cloud-Umgebung nennt Enathrax Laboratories "better inflecting redundant dynamic Cloud" ("birdCloud").

## Analyse der Vorteile, Chance und Risiken von Cloud Computing für AAL
Es ist eine gewisse Tendenz von Unternehmen, welche ihre gesamte oder grosse Teile ihrer IT-Infrastruktur in die Cloud-Umgebung von verschiedensten Cloud Computing Anbietern verschieben, erkennbar. Im Nachfolgenden sollen unter anderem die Vor- und Nachteile dieser Bewegung in die Cloud näher beleuchtet werden:


### Vorteile & Nachteile der aktuellen On-Premises-Lösung "aapCloud"
Die aktuelle IT-Infrastruktur bzw. die aktuelle "On-Premises-Lösung" ("On Premises" = "Auf dem Gelände", meint also eine Infrastruktur, welche sich an einem physikalischen Standort unter Kontrolle des Infrastruktur-Eigentümers befindet) bietet besonders im Hinblick auf die Datenhaltung einige Vorteile.

Die aktuelle "On-Premises-Lösung" (Name: aapCloud) befindet sich am geografischen Standort Winterthur im Kanton Zürich im Staat Schweiz. Folglich kann, besonders interessant und wichtig im Hinblick auf die [Weisung zur Datenklassifizierung (#AALD-DC-01)](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md), garantiert werden, dass die abgespeicherten Daten in der Schweiz und somit auch unter schweizer Rechtsprechung verbleiben. Dies bietet den Vorteil, dass Daten sämtlicher Klassifizierungsstufen in der aapCloud abgelegt und verarbeitet werden dürfen.

Diese Garantie der Datenhaltung ist sehr gut vereinbar mit den beiden [Werten von AAL "Gesetzestreue" sowie "Vertrauen und Kontrolle"](#unternehmenswerte), da durch die Datenhaltung in der Schweiz der Aufwand um die Einhaltung von Gesetzen sicherzustellen, minimiert werden kann (es muss lediglich auf die rechtliche Situation in der Schweiz geachtet werden, andere Staaten sind nicht relevant), und die Daten sich auch unter physischer Kontrolle von AAL befinden und somit eine bessere Überwachbarkeit der Umsetzung von Weisungen gewährleistet ist.

Ein grosser Nachteil der gegenwärtigen Lösung aapCloud ist jedoch die fehlende Flexibilität: So ist im Gegensatz zu Cloud-Umgebungen der verfügbare Speicherplatz beispielsweise begrenzt und muss falls nötig mühsam manuell mit entsprechender Hardware erweitert werden. 

Weiter fehlt AAL mit aapCloud selbstredend auch eine globale IT-Infrastruktur. Anstelle einer globalen Bereitstellung von Inhalten via CDN (Content Delivery Network) müssen abgerufene Daten aus dem Datencenter am Standort Winterthur übers Internet zum abrufenden User geroutet werden. Dieses Fehlen einer globalen Infrastruktur kann teilweise dazu führen, dass Daten, welche zwar der Öffentlichkeit zur Verfügung gestellt wurden, nur sehr selten abgerufen werden, da lange Downloadzeiten zu verzeichnen sind wenn beispielsweise ein Webseitenbesucher aus den Vereinigten Staaten von Amerika solche Daten, gespeichert in der aapCloud in Winterthur, abrufen möchte. In dieser Hinsicht kann AAL also dem [Unternehmenswert "Respekt"](#unternehmenswerte) hinsichtlich den Transparenzbemühungen nicht angemessen gerecht werden.

Mit dem Fehlen eines weiteren Datencenters geht auch ein höheres Ausfallsrisiko aufgrund der fehlenden Redundanz einher. Sämtliche Daten von AAL werden in der aapCloud, am geografischen Standort Winterthur, gespeichert und ein Backup sämtlicher Daten der aapCloud wird manuell erstellt sowie ebenfalls am Standort Winterthur auf externen Datenträgern abgelegt. (weitere Informationen siehe Abschnitt ["Bestehende IT-Infrastruktur"](#bestehende-it-infrastruktur))
Diese Vorgehensweise erlaubt also keine kurzfristige Bereitstellung von Ersatzinfrastruktur im Falle eines Ausfalles aufgrund äusserer Einwirkung und so wäre beispielsweise auch die Webseite von AAL nicht mehr aufrufbar, was sicherlich zu einem Vertrauensverlust der Öffentlichkeit und infolge zu einem nicht unerheblichen Reputationsschaden führen würde. Auch kann beispielsweise im Falle einer Naturkatastrophe davon ausgegangen werden, dass auch die angefertigten Backups/Sicherungskopien diesem Ereignis zum Opfer fallen könnten. Folglich ist aapCloud für die Speicherung von Daten des Wiederherstellungsschutzgrades ``G3 - DISASTER RESISTANT BACKUP`` (siehe [Weisung #AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md)) ungeeignet.



#### Kurzübersicht - Vorteile & Nachteile der aktuellen On-Premises-Lösung "aapCloud"
Vorteile:
- Kontrolle über Datenhaltung 
    - Gesetzgebung klar, da Standort klar definiert -> [Unternehmenswert "Gesetzestreue"](#unternehmenswerte)
    - Daten gelangen nicht in die Hände von unbefugten Drittparteien -> [Unternehmenswert "Vertrauen & Kontrolle"](#unternehmenswerte)
    - Einhaltung der internen Weisungen kann einfacher überwacht werden -> [Unternehmenswert "Vertrauen & Kontrolle"](#unternehmenswerte)
- Speicherung und Verarbeitung der Daten sämtlicher Klassifizierungsstufen möglich (siehe entsprechende [Weisung #AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md))

Nachteile:
- Begrenzte Ressourcen (z.B. begrenzter Speicherplatz)
- Langsames Bereistellen von Inhalten übers Internet
- Hohes Ausfallrisiko durch fehlende Redundanz der Ressourcen, da ein einziger geografischer Standort
- Daten des Wiederherstellungsschutzgrades ``G3 - DISASTER RESISTANT BACKUP`` ([Weisung #AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md)) sind nicht für die Ablage in aapCloud geeignet.


### Definition Cloud-Computing
Amazon Web Services "AWS", einer der grössten Anbietern von Cloud-Computing Dienstleistungen, definiert "cloud computing" in einem [Whitepaper](https://docs.aws.amazon.com/pdfs/whitepapers/latest/aws-overview/aws-overview.pdf#introduction) wie folgt:

> Cloud computing is the on-demand delivery of compute power, database, storage, applications, and
> other IT resources through a cloud services platform via the internet with pay-as-you-go pricing. 
> [...]
> Cloud computing provides a simple way to access servers, storage, databases and a broad set of
> application services over the internet. A cloud services platform such as Amazon Web Services owns and
> maintains the network-connected hardware required for these application services, while you provision
> and use what you need via a web application.

Folglich können beispielsweise Server in der Cloud betrieben werden, der Kunde verwaltet das Betriebssystem des Servers und ist für alles auf den darüberliegenden Ebenen verantwortlich, jedoch stellt der Cloud-Anbieter die Hardware, die Verbindungs- und Kontrollmöglichkeiten sowie das Rechenzentrum zu Verfügung.

**Im Rahmen dieses Projekts bezeichnet der Begriff "Cloud" eine Umgebung wie im obrigen Auszug aus erwähntem AWS-Whitepaper definiert. Wird explizit der Begriff "aapCloud" verwendet, ist darunter die bestehende "On-Premises"-Infrastruktur von AAL, definiert im Abschnitt ["Bestehende IT-Infrastruktur"](#bestehende-it-infrastruktur), zu verstehen.**

#### Cloud-Computing Modelle
Aufgrund der simplen Tatsache, dass unterschiedliche Kunden unterschiedliche Bedürfnisse sowie unterschiedlich weit entwickeltes Fachwissen in der Informationstechnik mitbringen, existieren hauptsächlich drei verschiedene "Cloud-Computing Modelle". Diese unterscheiden sich in der Tiefe der Konfigurationsebene für den Nutzer.

Nachfolgend werden die entsprechenden drei Modelle, inklusive einer kurzen Beschreibung sowie einer Auswahl einiger Cloud-Produkte von verschiedenen Anbietern aufgeführt:

**Infrastructure as a Service (IaaS)**
- Hardware-nächste Ebene im Vergleich zu PaaS und SaaS
- Bietet grundlegende Funktionen und Services wie Netzwerkkonnektivität, virtuelle Hardware oder Speicheroptionen, ähnelt sehr der bestehenden "On-Premises-Infrastruktur", Hardware und Virtualisierung werden aber kontrolliert, überwacht und bereitgestellt durch Cloud-Anbieter
- AWS S3, AWS VPC, Google Cloud Compute Engine, Google Cloud als Ganzes, AWS als Ganzes

**Platform as a Service (PaaS)**
- Bietet eine Plattform um die Applikationen und den Code des Kunden betreiben zu können. Der Cloud-Anbieter kümmert sich um die Bereitstellung dieser Plattform also um die Konfiguration der Betriebssysteme/Laufumgebungen und sämtlicher darunter liegender Infrastruktur.
- Google Cloud App Engine, Google Cloud Cloud Run, AWS Elastic Beanstalk

**Software as a Service (SaaS)**
- Vollständige Applikation bereitgestellt und verwaltet durch den Cloud-Anbieter. Dieser kümmert sich um die Bereitstellung dieser Applikation selsbt sowie sämtlicher darunter liegender Infrastruktur.
- Google Workspace (GMail, Docs, Sheets etc.), AWS WorkMail

#### Cloud Bereitstellungsmodelle
Je nach den Bedürfnissen des Kunden, beispielsweise im Hinblick auf die Datenhaltung, bieten sich unterschiedliche sogenannte "Bereitstellungsmodelle" an. Diese Bereitstellungsmodelle werden im Nachfolgenden aufgeführt, inklusive einiger weiteren Angaben zum jeweiligen Modell:

**Cloud**
- Wird teilweis auch "public Cloud" genannt (kann auch "private Cloud" genannt werden, beispielsweise wenn Zugriff auf Services nur innerhalb eines isolierten Netzwerks möglich ist)
- Applikationen/Services/Ressourcen laufen vollständig in der Cloud/Umgebung eines entsprechenden Cloud-Anbieters
- Diese Ressourcen profitieren vollständig von den Vorteilen der Cloud, beispielsweise "pay-as-you-go" Preismodell

**Hybrid**
- Mischung aus Applikation/Services/Ressourcen welche in der Cloud laufen und solchen welche auf "On-Premises" Infrastruktur laufen
- Teilweise kann von Vorteilen der Cloud profitiert werden, beispielsweise die flexible Erstellung von Ressourcen
- Die Cloud-Infrastruktur wird häufig mit der On-Premises Lösung verbunden, beispielsweise via Site-to-Site VPN oder AWS Direct Connect 

**On-premises**
- Wird teilweise auch "private Cloud" genannt (Fliessende Grenzen zu "Private Cloud" bei einem Cloud-Anbieter, Hardware wird dort weiterhin durch Anbieter verwaltet)
- Gesamte Infrastruktur, Hardware, Datencenterumgebung etc. wird durch den Kunden selbst bewirtschaftet

### Vorteile einer möglichen Cloud-Umgebung (birdCloud) für AAL
Ein sehr hervorzuhebender Vorteil von Cloud-Angeboten bzw. Cloud-basierten Diensten und Applikationen ist die ihnen innewohnende Flexibilität: Ressourcen wie beispielsweise Webserver können bei Bedarf einfach und schnell bereitgestellt werden. Besonders im Hinblick auf den [Unternehmenswert "Einfachheit"](#unternehmenswerte) von AAL ist dieser unbestreitbare Vorteil hervorzuhebenden.

Die möglichen Kosteneinsparungen, welche durch die Nutzung von Cloud-Ressourcen erfolgen könnten, scheinen jedoch, nach genauer Betrachtung der internen Weisungen (insbesondere [Weisung #AALD-TPA-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/transparencypublicsoc.md) (Zitat: "Work to live, don't live to work")), für AAL nicht sonderlich relevant zu sein. Dennoch könnten die möglichen Kosteneinsparungen einer Cloud-Lösung, durch das Entfallen der Notwendigkeit der vorgängigen Anschaffung von Ressourcen und Nutzung des "pay-as-you-go"-Modells, für eine Cloud-Umgebung sprechen.

Weiter von Vorteil ist die globale Infrastruktur der meisten Cloud-Providern. Dadurch kann von "Content Delivery Networks" (CDNs) profitiert werden, um so beispielsweise die Webseite von AAL ([www.aerospacelabs.antliuma.com](https://www.aerospacelabs.antliuma.com/)) den Besuchern mit einer sehr hohen Geschwindigkeit zur Verfügung stellen zu können. Auch bieten Funktionen wie "Cross-Region-Replication" die Möglichkeit, Daten des Wiederherstellungsschutzgrades ``G3 - DISASTER RESISTANT BACKUP`` (siehe Weisung [#AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md)) in Rechenzentren auf der ganzen Welt abzulegen und so die enstprechenden Anforderungen an diesen Wiederherstellungsschutzgrad zu erfüllen.

Unbestreitbar ist auch, dass durch die Verwendung von Produkten eines Cloud-Anbieters die spätere Einrichtung sowie Verwendung von zukünftigen Services, welche für AAL möglicherweise von grossem Interesse sind, vereinfacht wird. (Beispiel: Einrichtung eines [AWS VPC-Netzwerks](https://aws.amazon.com/de/vpc/) erfolgt bereits zum jetztigen Zeitpunkt im Rahmen der birdCloud, spätere Einrichtung eines Services, welcher auf AWS VPC aufbaut, wird durch bereits bestehendes VPC-Netzwerk vereinfacht und beschleunigt.) 

#### Kurzübersicht - Vorteile einer möglichen Cloud-Umgebung (birdCloud) für AAL
- Einfache Bereitstellung/Erstellung von Ressourcen (wie z.B. Servern, Speicher etc.) -> [Unternehmenswert "Einfachheit"](#unternehmenswerte)
- Möglichkeiten der effektiveren Datenverteilung durch globale Infrastruktur ("CDNs")
- Sicherstellen der Umsetzung der definierten Massnahmen ([Weisung #AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md)) für Daten des Wiederherstellungsschutzgrades ``G3 - DISASTER RESISTANT BACKUP`` durch beispielsweise "Cross-Region-Replication"
- Schnellere Einrichtung zukünftiger Cloud-Services durch bereits vorhandene Infrastruktur(-teile)
- "Pay-as-you-go"-Preismodell

### Risiken einer möglichen Cloud-Umgebung (birdCloud) für AAL
Die Nutzung von Dienstleistungen und Infrastruktur, welche durch eine Drittpartei verwaltet und bereitgestellt werden, ist selbstredend immer mit einem Risiko hinsichtlich der Datensicherheit, Datenintegrität sowie der Verfügbarkeit (Confidentiality, integrity, availability (CIA)) verbunden. 

Aufgrund der Tatsache, dass AAL teilweise äusserst sensitive Daten speichert, bereitstellt oder sonstwie verarbeitet, stellt AAL entsprechend hohe Anforderungen an die Infrastruktur, welche zur Verarbeitung solcher Daten herangezogen wird. Nachfolgend wird auf die Risiken, welche mit der Nutzung einer Cloud-Umgebung, bereitgestellt durch eine Drittpartei, einhergehen, näher eingegangen.

#### Shared Responsibility
Bei der Nutzung von Dienstleistungen und Produkten eines Cloud-Anbieters wird zwangsläufig die bestehende physische Infrastruktur des Anbieters genutzt, welche die bezogenen Dienstleistungen und Produkte bereitstellt. Die bereitgestellten Dienstleistungen und Ressourcen werden jedoch durch den Kunden genutzt, um beispielsweise Daten abzulegen oder bereitzustellen. Um trotz dieser "geteilten Verwaltung" die Sicherheit der Infrastruktur des Anbieters und die Sicherheit der Daten des Kunden sicherstellen zu können, werden die entsprechenden Verantwortlichkeiten meist nach einem "Modell der geteilten Verantwortung" (Shared Responsibility) zwischen Anbieter und Kunde aufgeteilt.

Der Dienstleiter (Cloud-Anbieter) stellt in einem solchen "Shared Responsibility"-Modell beispielsweise die Sicherheit und Funktionstüchtigkeit der physichen Komponenten (z.B. Datencenter werden vor Einbruch, Hochwasser und anderen Einwirkungen geschützt bzw. Massnahmen werden zum Schutze vor diesen Bedrohungen ergriffen) und Virtualisierungsinfrastruktur sicher. Hierbei spricht man häufig auch von der Verantwortung für die "Security of the Cloud" (Sicherheit der Cloud).

Der Kunde hingegen ist beispielsweise für die Sicherheit seiner Daten innerhalb der Cloud selbst verantwortlich. Es kann also im Verantwortungsbereich des Kunden liegen, die korrekte Konfiguration von Firewall-Einstellungen, Betriebssystemen, Benutzer- und Identitätsmanagementfunktionen oder Verschlüsselungssystemen sicherzustellen. Hierbei wird oft von der Verantwortung für die "Security in the Cloud" (Sicherheit in der Cloud) gesprochen.

Je nach Cloud-Plattform respektive Cloud-Anbieter unterscheidet sich diese Teilung der Verantwortlichkeit in "Security in the Cloud" und "Security of the Cloud" leicht. Nachfolgend finden sich Verlinkungen auf entsprechende Ressourcen einiger Anbieter von Cloud-Plattformen:

- [Amazon Web Services (AWS)](https://aws.amazon.com/de/compliance/shared-responsibility-model/)
- [Microsoft Azure (Azure)](https://learn.microsoft.com/en-us/azure/security/fundamentals/shared-responsibility)
- [Google Cloud Plattform (GCP)](https://cloud.google.com/architecture/framework/security/shared-responsibility-shared-fate?hl=de)
- [Atlassian](https://www.atlassian.com/whitepapers/cloud-security-shared-responsibilities)

#### Nachteile einer Cloud-Umgebung (birdCloud) im Hinblick auf gesetzliche und vertragliche Anforderungen
Wie im Abschnitt ["Gesetzliche & Vertragliche Vorschriften & Weisungen"](#gesetzliche-vertragliche-vorschriften-weisungen) dieses Dokuments bereits erläutert wurde, bestehen einige Vertragsbeziehungen zwischen AAL und Drittparteien. Ist die Speicherung, Bereitstellung oder jegliche andere Form einer Verarbeitung von jeglichen Daten Gegenstand einer solchen Beziehung, ist AAL selbstredend an allfällige Vertragsbedingungen gebunden. Ein Grossteil der entsprechenden Vertragsbedingungen erlaubt die Verarbeitung von Daten lediglich innerhalb des europäischen Raumes, was sich somit auch direkt auf die mögliche Verarbeitung solcher Drittparteiendaten ("third party data") auswirkt. Die Komplexität der einzelnen Vertragsinhalte wäre dem [Unternehmenswert "Einfachheit"](#unternehmenswerte) nicht sehr zuträglich, wie ebenfalls bereits im Abschnitt ["Gesetzliche & Vertragliche Vorschriften & Weisungen"](#gesetzliche-vertragliche-vorschriften-weisungen) ausgeführt und aus diesem Grunde wurde eine einheitliche Weisung ([#AALD-PID-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/thirddata.md)) erlassen, welche sich an den striktesten Vertragsbedingungen ausrichtet und somit die Einhaltung sämtlicher Verträge hinsichtlich Datenschutz, Datenhaltung etc. vereinfacht. Die Risiken der Cloud-Umgebung birdCloud hinsichtlich vertraglichen Abmachungen werden im Abschnitt ["Nachteile einer Cloud-Umgebung (birdCloud) im Bezug auf interne Weisungen"](#nachteile-einer-cloud-umgebung-birdcloud-im-bezug-auf-gesetzliche-und-vertragliche-anforderungen) dieses Dokuments näher beleuchtet.

Da je nach Konfiguration der Cloud-Umgebung Daten in Rechenzentren der verschiedensten Hoheitsgebiete verarbeitet oder gespeichert werden, besteht hier das Risiko eines Kontrollverlusts über die Daten. Sobald Daten nicht mehr in Rechenzentren auf schweizer Hoheitsgebiet verarbeitet werden, gilt es die Gesetzgebung des Staates, in welchem sich der geografische Standort des Rechenzentrums befindet, zu beachten. Insbesondere in Staaten mit "Hybridregime" oder gar "autoritärem Regime" (zu verstehen gemäss dem Demokratieindex des Magazins "The Economist") besteht die Gefahr willkürlicher Beschlagnahmungen von Hardware aus Rechenzentren und damit unter Umständen auch die Verwertung der darauf zu findenden Daten.

Aber auch Staaten jeglicher Regierungsformen bergen durch Nachrichtendienste und sonstige Strafverfolgungsbehörden ein Risiko für die Integrität und die Vertraulichkeit von Daten. Um Einsicht in Daten zu erhalten stehen solchen, wohlgemerkt mächtigen, Akteuren verschiedenste Techniken und Möglichkeiten zur Verfügung. So könnten Datenpackete mittels [BGP-Hijacking](https://www.cloudflare.com/learning/security/glossary/bgp-hijacking/) durch Knoten geleitet werden, welche sich unter Kontrolle eines erwähnten Akteurs befinden.
Oder staatlich garantierte Befugnisse können eingesetzt werden, um Anbietende von Cloud-Plattform dazu zu verpflichten, Behörden Zugriff auf Daten von Kunden zu gewähren (siehe auch [PRISM](https://de.wikipedia.org/wiki/PRISM)).

Insbesondere im Hinblick auf den Tätigkeitsbereich von AAL ist von einem grossen Interesse von staatlicher Seite an sensitiven Daten von AAL (insbesondere an Daten der Klassifizierungsstufen ``E3``, ``E4`` und ``E5`` (siehe dazu [#AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md))) (beispielsweise Informationen zur Steuerung von Satelliten oder Baupläne von Satelliten) auszugehen.

#### Nachteile einer Cloud-Umgebung (birdCloud) im Hinblick auf interne Weisungen
Um die Sicherheit sämtlicher interner Daten von AAL selbst und der Daten mit deren Verarbeitung AAL durch Dritte beauftragt wurde sicherzustellen, gibt es einige interne Weisungen (diese sind im Abschnitt ["Interne Vorschriften & Weisungen"](#interne-vorschriften-weisungen) dieses Dokuments bereits aufgelistet). Diese Weisungen gilt es selbstverständlich auch in einer möglichen Cloud-Umgebung zu beachten. 

Durch den Verlust der Kontrolle über die verwendete Hardware, welche im Falle der Verwendung einer Cloud-Umgebung vom Anbieter selbst verwaltet wird, geht auch ein gewisses Risiko für die Integrität, Sicherheit und Verfügbarkeit der Daten von AAL einher. Beispielsweise kann lediglich vertraglich durch den Cloud-Anbieter zugesichert werden, dass der physische Zugriff auf Hardware Dritten oder unvertrauenswürdigen Mitarbeitenden verwehrt bleibt. 

Besonders hinsichtlich Daten von Drittparteien ist Vorsicht bei der Nutzung von Cloud-Umgebungen geboten. Die entsprechende Weisung ([#AALD-PID-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/thirddata.md)) führt entsprechend auch aus: "Drittparteiendaten sind als äusserst sensitiv anzusehen [...]". Weiter wird in der eben erwähnten Weisung ausgeführt: "Drittparteiendaten [...] werden [...] immer der Klassifizierungsstufe E3 - CONFIDENTIAL zugewiesen.". 

Weiter gestaltet sich in einer Cloud-Umgebung die Durchsetzung der internen Weisungen (beispielsweise zur Datenklassifizierung oder zum Umgang mit Drittparteiendaten) schwieriger als auf On-Premises-Infrastruktur da auf die Services und Dienste des Cloud-Anbieters zurückgegriffen werden muss und die Kontrolle über die technische Funktionstüchtigkeit und Zuverlässigkeit dieser Services dem Cloud-Anbieter obliegt. AAL hat also keine technische Garantie, dass Dienste zum Identitätsmanagement des Cloud-Anbieters zuverlässig die durch AAL konfigurierten Richtlinien um- und druchsetzen. 

#### Kurzübersicht - Risiken einer möglichen Cloud-Umgebung (birdCloud) für AAL
- Kontrollverlust über sensitive Daten
- Gefahr durch Geheimdienste/Strafverfolgungsbehörden des Hoheitsgebietes auf welchem sich die Infrastruktur des Cloud-Anbieters befindet
- Gefahr der Schädigung durch verärgerte Mitarbeitende des Cloud-Anbieters etc.
- Gefahr bei Übertragung der Daten in die Cloud (z.B. BGP-Hijacking)
- Umsetzung von internen Weisungen bedingt funktionierende IAM-Services des Cloud-Anbieters

#### Schlussfolgerungen - Risiken einer möglichen Cloud-Umgebung (birdCloud) für AAL
Schlussendlich lässt sich zusammenfassen, dass die Nutzung von Cloud-Umgebungen Risiken birgt, diesen aber mit einer lückenlosen und konsequenter Durch- und Umsetzung der internen Weisungen (insbesondere der Weisung zur Klassifizierung von Daten ([#AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md))) begegnet werden kann.
Insbesondere dürfen sehr sensitive Daten von AAL nicht in der geplanten Cloud-Umgebung "birdCloud" abgelegt oder verarbeitet werden. Die Weisung #AALD-DC-01 wurde bereits entsprechend angepasst.

Drittparteiendaten dürfen in der Cloud-Umgebung "birdCloud" verarbeitet und gespeichert werden. Dazu findet sich nachfolgend der Auszug aus der Weisung [#AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md) zur Klassifizierungsstufe E3 - CONFIDENTIAL (welcher Drittparteiendaten zugeordnet werden)(Stand 26.09.2023):


> E3 - CONFIDENTIAL
> 
> - Daten dieser Stufe sind sämtlichen internen Mitarbeitenden zugänglich. Auf Daten dieser Stufe darf auch in Daten der Stufe E2 verwiesen werden, jedoch dürfen keine als E3 eingestuften Inhalte in Daten der Stufe E2 vorhanden sein.
> - Daten dieser Stufe dürfen auf den verwalteten private sowie public Cloud-Umgebungen von AAL abgelegt werden. Namentlich in der "birdCloud" sowie der "aapCloud". Die Daten müssen innerhalb der private oder public Cloud-Umgebungen von AAL nicht separat verschlüsselt werden. Daten dieser Stufe dürfen über die verwalteten Kommunikationsplattformen von AAL geteilt werden.
> - Bei der Ablage in einer private oder public Cloud-Umgebung von AAL muss der geografische Standort des Datencenters, in welchem die Daten abgespeichert oder verarbeitet werden sollen, sich zwingend innerhalb des europäischen Raumes befinden. Keinesfalls dürfen Daten dieser Stufe im US-Amerikanischen Raum gespeichert oder verarbeitet werden.



Drittparteiendaten können also gemäss dieser Weisung auch in "Public Cloud-Umgebungen" gespeichert und verarbeitet werden, da AAL durch die Verträge mit Drittparteien zwar zusichern musste, dass Drittparteiendaten nur im europäischen Raume verarbeitet werden, aber bei der Auswahl der verwendeten Infrastruktur Handlungsfreiheit besitzt.

Da die Verarbeitung oder Speicherung von Daten in der Cloud-Umgebung eines Anbieters mit einem gewissen Kontrollverlust einhergeht und lediglich vertragliche Garantien für den sorgsamen Umgang des Anbieters mit den Daten von AAL bestehen, dürfen sehr sensitive / besonders schutzwürdige Daten niemals in eine Cloud-Umgebung, verwaltet durch einen entsprechenden Cloud-Anbieter, gelangen. Die Weisung zur Datenklassifizierung (#AALD-DC-01) wurde entsprechend angepasst.

Folglich wird an dieser Stelle AAL empfohlen, eine hybride Nutzung der Cloud-Dienstleistungen zu praktizieren (Cloud Bereitstellungsmodel "Hybrid"). Folglich können einige Daten und Ressourcen von "aapCloud" in die "birdCloud" migriert werden, die "aapCloud" bleibt jedoch weiterhin zentraler Bestandteil der Infrastruktur von AAL und die "birdCloud" wird lediglich für einige ausgewählte und klar definierte Anwendungen verwendet. Die genaue Definition dieser Anwendungen erfolgt im Verlaufe der vorliegenden Dokumentation. 

Um eine möglichst grosse Flexibilität sicherzustellen und beispielsweise die Segmentierung von Netzwerk-Ressourcen (wie Web-Servern, internen Kommunikationsplattformen etc.) oder die Nutzung eines VPN-Servers mit möglichst vielen Konfigurationsmöglichkeiten zu ermöglichen, sollte im Rahmen des Projektes birdCloud auf viele Produkte respektive Services zurückgegriffen werden, welche sich dem 
Cloud-Computing Model "Infrastructure as a Service (IaaS)" zuordnen lassen.

## Auswahl eines Anbieters von Cloud Computing Dienstleistungen
Weltweit gibt es eine grosse Auswahl an Anbietern von Cloud Computing Dienstleistungen. Im Nachfolgenden wird anhand von verschiedensten Markmalen die Wahl des Cloud-Anbieters, welcher die Infrastruktur für die Cloud-Umgebung von AAL "birdCloud" bereitstellen wird, getroffen:

### Definition der relevanten Merkmale inkl. Gewichtung

- Umfang der Infrastruktur/Standorte Rechenzentren (besonders im Hinblick auf Daten mit Wiederherstellungsschutzgrad G3) (Gewichtung 1x)
    - Möglichkeiten für Speicherung von Ressourcen innerhalb von Europa (siehe Klassifizierungsstufe E3) (Gewichtung 2x)
    - Möglichkeiten für Speicherung von Ressourcen innerhalb der Schweiz (Gewichtung 1x)
- Umfang der Dienstleistungen (Gewichtung 1x)
- Möglichkeiten zur Automatisierung von Abläufen / Erstellung von Ressourcen (Gewichtung 1x)
- Flexibilität / Möglichkeiten tief in Cloud-Umgebung einzugreifen / IaaS (Gewichtung 2x)
    - Bereite Auswahl an Betriebssystemen auf Servern (VPS und dedizierte Server) (Gewichtung 2x)
    - Möglichkeiten zur Erhöhung der Sicherheit in der Cloud (IAM-Services, ACLs etc.) (integriert in Punkt "IaaS")
- Möglichkeiten hybride Nutzung der Cloud (um Compliance-konforme Aufteilung der Daten zu ermöglichen (Daten bis und mit Klassifizierungsstufe E3 in birdCloud, Daten der Klassifizierungsstufen E4 und E5 in aapCloud)) (Gewichtung 2x)
    - Möglichkeiten aapCloud (On-Premises) und birdCloud (Cloud) via Netzwerk effizient zu verbinden (BGP Peering, Direkter Link etc.) (Gewichtung 1x)
- Transparenz des Cloud-Anbieters (Bereitstellung von Whitepapers und Informationsunterlagen zur eigenen Infrastruktur, Vorweisen von Compliance-Zertifizierungen) (Gewichtung 3x)
- Verfügbarkeit von Dokumentationsunterlagen und Handbüchern (Gewichtung 2x)
    - Möglichkeiten von Mitarbeiter-Schulungen/Weiterbildungen/Zertifizierungen (Gewichtung 1x)
- Hohe Skalierbarkeit (Gewichtung 1x)

### Vergleich von verschiedenen Anbietern

|  | Amazon Web Services Inc. (AWS) | _Punkte AWS_ | Scaleway SAS (Scaleway) | _Punkte Scaleway_ | Google Cloud Plattform (GCP) | _Punkte GCP_ | OVHcloud (OVH) | _Punkte OVH_ | Microsoft Azure (Azure) | _Punkte Azure_ |
-------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- |
Umfang Standorte Rechenzentren  | 32 "Regions" (23 Staaten)   | 8/10  | 3 "Regions" (3 Staaten) | 2/10  | 37 "Regions" (24 Staaten)   | 9/10  | 8 Staaten   | 4/10   | 42 "Regions" (25 Staaten) | 10/10  |
Standorte Rechenzentren innerhalb Europas   | 8 "Regions" (8 Staaten)   | 8/10  | 3 "Regions" (3 Staaten) | 4/10  | 11 "Regions" (10 Staaten)   | 9/10  | 4 Staaten   | 5/10   | 18 Regions (10 Staaten) | 10/10  |
Standorte Rechenzentren innerhalb der Schweiz   | 1 "Region" (eu-central-2)   | 4/10  | 0 "Regions" | 0/10  | 1 "Region" (europe-west6)   | 4/10  | 0 "Regions"   | 0/10   | 2 "Regions" (Switzerland North & Switzerland West) | 5/10  |
Umfang Dienstleistungen   | 200+ Services   | 10/10  | 33 Services | 4/10  | 100+ Services   | 8/10  | 80+ Services   | 6/10   | 200+ Services | 10/10  |
Möglichkeiten Automatisierung   | Ja (z.B. CloudFormation, Terraform, Möglichkeiten mittels Lambda & entsprechenden Frameworks)   | 10/10  | Ja (z.B. Terraform) | 6/10  | Ja (z.B. Terraform, Cloud Deployment Manager)   | 9/10  | Ja (z.B. Terraform)   | 6/10   | Ja (z.B. Terraform, Azure Resource Manager) | 9/10  |
IaaS / Grosse Flexibilität   | VPC, EC2, EFS, EBS, S3, Direct Connect, BGP Peering, Route 53, BYOIP ...   | 10/10  | Block/Object Storage, VPC, Virtual Instances, Bare Metal | 4/10  | Interconnect, Compute Engine, VPC, Cloud Storage, DNS, BYOIP, ...   | 10/10  | VPS, Bare Metal, BYOIP, private Networks, ...   | 5/10   | VMs, Blob Storage, DNS, private 5G, VPN, ExpressRoute, BYOIP | 10/10  |
Auswahl Betriebssysteme für VPS / ded. Server   | AMIs von AWS, Drittanbietern (extrem grosse Auswahl -> sehr etabliert), AWS Community sowie benutzerdefinierte Images  | 10/10  | GNU/Linux-Distros, InstantApps, Windows -> Kleine Auswahl | 5/10  | öffentliche (z.B. GNU/Linux-Distros, Windows etc.) und benutzerdefinierte Images   | 7/10  | öffentliche (z.B. GNU/Linux-Distros, Windows) und benutzerdefinierte Images   | 6/10   | öffentliche (z.B. GNU/Linux-Distros, Windows etc. ) und benutzerdefinierte Images  | 7/10  |
Möglichkeiten hybrider Nutzung   | Direct Connect, Direct Peering, Subnetze in VPCs, Verwenden von Industriestandards (z.B. S3) bzw. sehr verbreitet und somit auch in viele Applikationen nativ integriert, ...   | 10/10  | Private Netzwerke in der Cloud | 3/10  | Interconnect, VPN, Direct Peering, ...  | 9/10  | Connect Direct, VPN auf VPS   | 6/10   | VPN, Direct Connect, Peering, ... | 9/10  |
Möglichkeiten Netzwerkkonnektivität aapCloud <-> birdCloud   | Peering, Direct Connect, VPN, etc.   | 10/10  | VPN auf VPS | 3/10  | Interconnect, VPN, Peering, etc.   | 10/10  | Connect Direct, VPN auf VPS   | 6/10   | VPN, Direct Connect, Peering, etc. | 10/10  |
Transparenz des Cloud-Anbieters | Viele Whitepaper und Handbücher verfügbar (z.B. zu Datenschutz- und Compliance-Themen)   | 9/10  | Sehr transparent zur eigenen Infrastruktur (Schemata z.T. öffentlich), eher wenige ausführliche Whitepapers | 8/10  | Einige Whitepaper und Handbücher verfügbar (z.B. zu Datenschutz- und Compliance-Themen)   | 8/10  | Eher wenige ausführliche Whitepapers   | 6/10   | Eher wenige ausführliche Whitepapers, sehr unübersichtliche Strukturierung der entsprechenden Informationen | 7/10  |
Verfügbarkeit von Dokumentationsunterlagen | Viele Dokumente von AWS, How-Tos von AWS, How-Tos von Drittpersonen auf Webseiten, Handbücher, Videos von AWS sowie Drittpersonen etc.    | 10/10  | Dokumente und How-Tos verfügbar, eher weniger Content von Drittpersonen | 8/10  | Dokumente, How-Tos, Videos und Handbücher verfügbar, eher weniger Content von Drittpersonen   | 9/10  | Dokumente, How-Tos und Handbücher verfügbar, eher weniger Content von Drittpersonen   | 8/10   | Dokumente, How-Tos und Handbücher verfügbar, eher weniger Content von Drittpersonen, Teilweise etwas unübersichtliche Strukturierung der Learning-Ressourcen | 8/10  |
Learning-Angebote | Kostenlose AWS Academy und verschiedenste kostenpflichtige Zertifizierungen, Sehr ausführliche und umfassende Handbücher verfügbar, industrieweit sehr anerkannt   | 10/10  | Learning und Zertifzierungen auf Anfrage möglich | 7/10  | Learning Ressourcen teils kostenlos teils kostenpflichtig, kostenpflichtige Zertifizierungen verfügbar | 8/10  | Learning-Ressourcen und Zertifizierungen verfügbar   | 7/10   | Learning-Ressourcen und Zertifizierungen verfügbar | 8/10  |
Hohe Skalierbarkeit | Services wie Kubernetes, Containerumgebungen etc. verfügbar, neue AWS Services in Aussicht (z.B. Satelliteninternet), Neue Regions/Zonen/Datencenter in Aussicht, Services mit keinen Limits (z.B. S3)   | 10/10  | Services wie Kubernetes, Containerumgebungen etc. verfügbar | 6/10  | Neue Regions/Zonen/Datencenter in Aussicht, Services mit keinen Limits (z.B. S3), Services wie Kubernetes, Containerumgebungen etc. verfügbar    | 9/10  | Services mit keinen Limits (z.B. S3), Services wie Kubernetes, Containerumgebungen etc. verfügbar   | 7/10   | Neue Regions/Zonen/Datencenter in Aussicht, Services mit keinen Limits (z.B. S3), Services wie Kubernetes, Containerumgebungen etc. verfügbar | 9/10  |
**Totale Punktzahl (vor Gewichtung)** |    | 119/130  |  | 60/130  |    | 109/130  |    | 72/130   |  | 112/130  |
**Totale Punktzahl (inkl. Gewichtung)** |    | 185/200  |  | 100/200  |    | 169/200  |    | 114/200   |  | 170/200  |


_Sämtliche Angaben und Zahlen stammen von den jeweiligen offiziellen Webseiten der Cloud-Anbieter. Die Daten wurden am 03.10.2023 abgerufen und wiederspiegeln somit den Stand zu diesem Zeitpunkt. "Regions"/Standorte sowie Services, welche erst in Kürze verfügbar sein werden, jedoch zum aktuellen Zeitpunkt noch nicht genutzt werden können, wurden zu den entsprechenden Einträgen in dieser Tabelle nicht hinzugezählt._

#### Wahl und Begründung der Wahl eines Anbieters
Wie aus der vorhergehenden Tabelle zu entnehmen ist, weisst Amazon Web Services (AWS) die höchste Punktzahl auf und ist somit der Cloud-Anbieter, welcher für die Bereitstellung der Cloud-Umgebung "birdCloud" gewählt wird. AWS bietet eine sehr breite Auswahl an Cloud-Computing-Dienstleistungen, von Services zur direkten Anbindung der On-Premises-Infrastruktur via Glasfaser zu hoch-skalierbaren Datenspeichern. AWS stellt weiter eine grosse Menge an Datenblättern, Dokumentationen sowie Handbüchern zur Implementierung und Nutzung der Services bereit. Bei der Wahl des Cloud-Anbieters war im Falle des Projekts "birdCloud" unter anderem diese "Transparenz" bei der Bereitstellung der eben genannten Dokumenten ausschlaggebend.

## Benötigte Services & Ressourcen
Nachfolgend werden einige Anforderungen an die Dienstleistungen, welche durch AWS bereitgestellt und in der birdCloud Verwendung finden werden, definiert:

### Technische Anforderungen an die Services
- Möglichkeit grosse Mengen an Daten abzulegen (Dokumente, Bildmaterial etc.)
- Möglichkeit Daten der Öffentlichkeit bereitzustellen (gemäss [Weisung #AALD-TPA-01 (Transparenz gegenüber der Öffentlichkeit)](/corporate_docs/transparencypublicsoc.md))
- Möglichkeit Mitarbeitenden von AAL Plattformen zur Zusammenarbeit bereitzustellen
- Möglichkeit Webinhalte via CDN (Content-Delivery-Network) bereitzustellen
- Möglichkeit Sicherungskopien von Inhalten der birdCloud mit Wiederherstellungsschutzgrad G2 oder G3 (siehe [Weisung #AALD-DC-01 (Datenklassifizierung)](/corporate_docs/datenklassifizierung.md))
- Möglichkeit DNS-Records für Domains von AAL via DNS bereitzustellen

### Compliance-bedingte Anforderungen an die Cloud-Umgebung
- Möglichkeit der Erstellung von isolierten Netzerken mit darin enthaltenen Ressourcen, welche nur durch das Netzen eines VPNs (Virtual Private Network) genutzt werden können
- Möglichkeit Daten des Wiederherstellungsschutzgrades G3 entsprechend [Weisung #AALD-DC-01 (Datenklassifizierung)](/corporate_docs/datenklassifizierung.md) zu speichern
- Möglichkeit mittels zentralem IAM (Identity and Access Management) Zugangskonten zur AWS Web-Managementkonsole für Mitarbeitende von AAL zu erstellen

### Migrierbare Services/Funktionen (aapCloud -> birdCloud)
Folgende Inhalte/Ressourcen können, da durch die enstprechenden Klassifizierungsstufen zugelassen, aus der aapCloud in die neu geschaffene birdCloud migriert werden:
- Inhalte aus Objektspeicher (Bildmaterialien, Dokumente etc.) -> Amazon S3
- Webinhalte -> EC2 / S3 / CloudFront
- DNS-Zonen für Domains von AAL -> Route 53

### Neue Services/Ressourcen
Folgende Ressourcen werden in der birdCloud neu geschaffen, da deren Notwendigkeit durch die Schaffung der birdCloud entstanden ist:
- VPN-Server -> EC2 / VPC
- Backup-Lösung -> AWS Backup
- Kollaborationsplattform -> EC2 Instanz (Nextcloud)
- Erstellung von Benutzerkonten für Mitarbeitende von AAL -> IAM

### Definition der Services in der birdCloud
Folgende Services von AWS werden zur Umsetzung des Projektes birdCloud voraussichtlich genutzt:

- Speicherung
    - Amazon S3 (Simple Storage Service)
    - Amazon EBS (Elastic Block Store)
- Computing
    - Amazon EC2 (Elastic Compute Cloud)
- Governance
    - AWS IAM (Identity and Access Management)
    - AWS Organizations
- Netzwerke
    - Amazon Route 53
    - Amazon VPC (Virtual Private Cloud)
    - Amazon CloudFront
- Datensicherungen
    - AWS Backup
- Automatisierung
    - AWS CloudFormation

### Kostenschätzung
Obwohl, wie [bereits vorgängig ausgeführt](#vorteile-einer-m%C3%B6glichen-cloud-umgebung-birdcloud-f%C3%BCr-aal), die Kosten des Projekts birdCloud für AAL nur eine sehr untergeordnete Rolle spielen, wird im Nachfolgenden eine grobe Schätzung der zu erwarteten Kosten der Cloud-Umgebung birdCloud aufgeführt. Es jedoch erwähnt, dass die prognostizierten Kosten sind jedoch keineswegs als vollständig betrachtet werden dürfen. Einerseits besteht die Möglichkeit, dass zukünfitg weitere AWS-Services durch AAL genutzt werden (so ist beispielsweise damit zu rechnen, dass AWS zukünftig Netzwerkanbindungen via Satellit anbieten könnte (siehe dazu [Project Kuiper](https://www.aboutamazon.com/what-we-do/devices-services/project-kuiper))) sowie das Ausmass der Nutzung der birdCloud durch Mitarbeitende von AAL schwankt (z.B. Erstellug neuer EC2-Instanzen, Nutzung von S3-Speicherplatz, Erhöhung des Datenverkehrs etc.). Diese Faktoren führen zwangsläufig dazu, dass die unten aufgeführte Kosteneinschätzung nicht mehr zutrifft. Lizenzkosten für Software dürften bis auf Ausnahme der Software "OpenVPN" nicht anfallen, da hauptsächlich auf freie Software, also Software unter einer GPL oder vergleichbaren Lizenz, in der birdCloud gesetzt wird.

Diese Kosteneinschätzung beinhaltet keine Kosteneinschätzung für die aufgewendete Arbeitszeit für die Umsetzung des Projekts birdCloud, da AAL hierbei auf interne Mitarbeitende zurückgreifen kann und vermutlich auch wird und die Entlöhnung somit bereits sichergestellt ist, sowie keine Einschätzung für die Kosten der theoretischen Planung (deren Produkt diese hier vorliegende Dokumentation ist) da diese Arbeit dem bereits existierenden Basisvertrag zwischen AAL und Enathrax Laboratories untersteht (siehe auch [Auftragsschreiben von AAL an Enathrax Laboratories](#auftragsschreiben-von-aal-an-enathrax-laboratories)).


Folgende Kosten sind monatlich zu ungefähr erwarten:

- S3 Bucket (500 GB) * 4
- EC2-Instanzen * 6
- AWS Backup (ca. 600 GB) * 5
- CloudFront 

=> CHF 250

(Grobe Schätzung, da On-Demand-Bezahlmodell sind von Monat zu Monat unterschiedliche Kosten zu erwarten)

#### Möglichkeiten zur Kostenoptimierung
AAL kann bei Bedarf verschiedene Schritte unternehmen, um die Kosten der birdCloud zu senken. So bietet AWS die Möglichkeit für EC2-Instanzen, welche voraussichtlich für einen gewissen Zeitraum benötigt werden, "Saving Plans" zu nutzen, welche zur Nutzung der EC2-Instanz für eine bestimmte Zeitdauer verpflichten, im Gegenzug jedoch niedrigere Preise (als verglichen mit dem "On-Demand"-Preismodell für EC2-Instanzen) bieten. 
Auch lassen sich Daten, welche im Objektspeicher S3 abgespeichert sind, in verschiedene Speicherklassen verschieben: Daten auf welche nur äusserst selten zugegriffen wird, könnten so von der Speicherklasse "S3 Standard" in die Klasse "S3 Standard – IA (Infrequent Access)" verschoben werden.

Grosse Kosteneinsparungen können auch erreicht werden durch die Nutzung einer EC2-Instanz, welche als "Router" bzw. "NAT-Gateway" dient und so die durch AWS in VPCs bereitgestellten NAT-Gateways teilweise ersetzen kann. 

Weiter empfiehlt es sich in der AWS Cloud den Service "AWS Budgets" zu nutzen, um so Mitteilungen via Mail oder weiteren Kommunikationswegen über die zu erwartenden Kosten per Ende Monat sowie die genutzten Ressourcen zu erhalten.

## Datenschutz & Klassifizierungsstufen
Wie in dieser Dokumentation bereits mehrfach betont, besitzt Schutz der Daten von AAL und Drittparteien, welche Daten AAL anvertrauen, höchste Priorität. Somit muss bei der Ausarbeitung des Projekts birdCloud dieser Punkt ausführlich betrachtet werden, wie in dieser Dokumentation in verschiedenen Abschnitten bereits geschehen. Nachfolgend wird ausgeführt durch welche Massnahmen der Schutz von AAL-Daten sowie Drittparteiendaten sichergestellt werden soll:

Wie bereits in der Weisung [#AALD-DC-01 (Datenklassifizierung)](/corporate_docs/datenklassifizierung.md) ersichtlich, dürfen lediglich Daten der Klassifizierungsstufen "E1 - PUBLIC", "E2 - INTERNAL" und "E3 - CONFIDENTIAL" in der birdCloud-Umgebung gespeichert oder verarbeitet werden. Dies umfasst auch temporäre Verarbeitungen (beispielsweise Nutzung des Text-to-Speech Services "Amazon Polly" für Texte der Klassifizierungsstufe E4). Daten der Klassifizierungsstufen "E4 - SECRET" und "E5 - TOP SECRET" dürfen unter keinen Umständen in die birdCloud gelangen. Auch die vorgängige Verschlüsselung solcher Daten auf der Infrastruktur von AAL und die anschliessende Speicherung dieser Daten in der birdCloud ist aufgrund der fehlenden Praktikabilität und Konsistenz untersagt. Durch die klare Anweisung, dass Daten der Klassifizierungsstufen E4 und E5 ausschliesslich in die aapCloud gelangen dürfen, ist eine gewisse Übersichtlichkeit und Konsistenz der Datenspeicherung gegeben und das Risiko eines Kontrollverlusts über hochsensitive Daten wird minimiert.

Weiter ist auch innerhalb der birdCloud eine klare Kennzeichnung der Klassifizierungsstufe der Daten unbedingt erforderlich:
So sollen Daten der Klassifizierungsstufe "E1 - PUBLIC" öffentlich zugänglich gemacht werden und müssen somit auch klar von den Daten höherer Klassifizierungsstufen getrennt werden. Für Daten der Klassifizierungsstufe "E3 - CONFIDENTIAL" gelten Einschränkungen bezüglich des "geografischen Standorts des Datencenters" und folglich muss auch hier eine klare Abgrenzung zu Daten der Stufe "E2 - INTERNAL" stattfinden. 

Innerhalb der AWS-CLoud findet die "Definition" des geografischen Standortes der Datencenter über "Regions" sowie "Availability Zones" statt. Am 23.10.2023 sind folgende Regionen verfügbar:

- Nordamerika
    - AWS GovCloud* (USA Ost)
    - AWS GovCloud* (USA-West)
    - Kanada (Zentral)
    - Nord-Virginia
    - Nordkalifornien 
    - Ohio 
    - Oregon 
- Südamerika
    - São Paulo
- Europa
    - Europa (Stockholm)
    - Frankfurt 
    - Irland 
    - London 
    - Mailand
    - Paris 
    - Spanien 
    - Zürich
- Naher Osten
    - Bahrain 
    - Tel Aviv
    - VAE
- Afrika
    - Kapstadt 
- Asien-Pazifik
    - Beijing**
    - Hongkong (SVZ)
    - Hyderabad
    - Jakarta 
    - Mumbai
    - Ningxia**
    - Osaka 
    - Seoul
    - Singapur 
    - Tokio
- Australien & Neuseeland
    - Melbourne 
    - Sydney 

_Die verwendeten Namen sind identisch mit den Bezeichnungen der Regionen durch AWS selbst und können aufgrund dieser Tatsache eine gewisse Inkonsistenz aufweisen_

_* AWS GovCloud ist lediglich für Regierungsbehörden sowie für deren Auftragnehmer verfügbar_ <br>
_** Aufgrund gesetzlichen Vorgaben ist die Nutzung der AWS-CLoud in der Volksrepublik China bestimmten Bestimmungen unterworfen_

Für Daten der Klassifizierungsstufe "E3 - CONFIDENTIAL" ist gemäss Weisung [#AALD-DC-01 (Datenklassifizierung)](/corporate_docs/datenklassifizierung.md) zwingend eine Region innerhalb Europa zu wählen, also eine der folgenden Regionen: 

- Europa (Stockholm)
- Frankfurt 
- Irland 
- London 
- Mailand
- Paris 
- Spanien 
- Zürich

Für Daten der Klassifizierungsstufen E1 und E2 ist die Region "Nord-Virginia" geeignet, da hier sämtliche Services von AWS verfügbar sind und neuartige Services meist priorisiert in dieser Region durch AWS verfügbar gemacht werden. Für Daten der Klassifizierungsstufe E3 erscheint die Region "Frankfurt" geeignet, da hier einerseits eine geografische Nähe zur Schweiz gegeben ist und andererseits eine grosse Verfügbarkeit von AWS-Services in dieser Region gegeben ist.

Innerhalb der Cloud-Umgebung werden diese beiden Regionen mit folgenden Kürzeln bezeichnet:

- Nord-Virginia -> us-east-1	
- Frankfurt -> eu-central-1	

AWS-Regionen sind jeweils noch weiter in "Availability Zones" oder kurz "AZ" eingeteilt. Diese Unterteilung lässt sich wie folgt vereinfacht darstellen:

```
|-> AWS-Cloud
  |-> Region XYZ
    |-> Availability Zone X
```

Availability Zones werden in der AWS-Managementkonsole auch, falls erforderlich, an die Kürzel von Regionen angehängt (z.B. "us-east-1a"). Damit wird eine übersichtliche Bezeichnung der Region und Availability Zone ermöglicht.

AWS selbst definiert Regionen und Availability Zones wie folgt: "Bei AWS sprechen wir oft von Regionen, einem physischen Standort, an dem wir Rechenzentrums-Cluster betreiben. [..] Eine Availability Zone (AZ) ist ein oder mehrere [..] Rechenzentren mit redundanter Stromversorgung, Vernetzung und Konnektivität in einer AWS Region"

_Quelle: [AWS: Regionen und Availability Zones](https://aws.amazon.com/de/about-aws/global-infrastructure/regions_az/)_


Um die in diesem Abschnitt bereits erwähnte, erforderliche klare Unterscheidung zwischen den Daten der Klassifizierungsstufen E1, E2 und E3 zu gewährleisten, wird  wenn immer möglich mithilfe von sogenannten "Meta-Tags" sowie mittels Benennung von Ressourcen (wie beispielsweise S3-Buckets) klar festgehalten, Daten welcher Klassifizierungsstufen in dieser Ressource verarbeitet bzw. gespeichert werden dürfen. Der entsprechende Abschnitt 4.1 der Weisung  [#AALD-DC-01 (Datenklassifizierung)](/corporate_docs/datenklassifizierung.md#41-schreibweise-von-klassifizierungsstufe-wiederherstellungsschutzgrad) ist selbstredend konsequent anzuwenden.

Die [Namenskonvention](#namenskonvention) findet bei der Benennung von Ressourcen in der birdCloud unverändert Anwendung und lediglich die jeweilige Klassifizierungsstufe wird der Benennung gemäss Namenskonvention vorangestellt. Ressourcen in der birdCloud werden also nach folgendem Schema benannt:

- ```birdcloud.aerospacelabs.antliuma.com```
- ```birdcloud-aerospacelabs-antliuma-com```
- ```birdcloud.aal.antliuma.com```
- ```birdcloud-aal-antliuma-com```
- ```*.[Kürzel Klassifizierungsstufe].birdcloud.aerospacelabs.antliuma.com```
- ```*-[Kürzel Klassifizierungsstufe]-birdcloud-aerospacelabs-antliuma-com```
- ```*.[Kürzel Klassifizierungsstufe].birdcloud.aal.antliuma.com```
- ```*-[Kürzel Klassifizierungsstufe]-birdcloud-aal-antliuma-com```

Beispiel:

```osaka.std.s3.us-east-1.g1.e1.birdcloud.aerospacelabs.antliuma.com``` <br>
_Daten, welche also in einer Ressource, welche nach vorangehendem Beispiel benannt wurde, abgespeichert werden, müssen die Klassifizierungsstufe E1 - PUBLIC aufweisen._

Um sicherzustellen, dass Daten der Klassifizierungsstufe E3 ausschliesslich internen (direkt angestellten) Mitarbeitenden von AAL zugänglich sind, ist mittels IAM (Identitätsmanagement) zumindest eine Einteilung der Nutzerinnen und Nutzer der birdCloud in externe und interne Personen vorzunehmen. Mit zugewiesenen Berechtigungen kann dann eine Zugriffkontrolle umgesetzt werden.

## Datenerhaltung & Wiederherstellungsschutzgrad - Backup-Systeme (auch Modul 143)
Neben dem Schutz von Daten gegen unberechtigte Zugriffe ist auch das Schützen der Daten vor Verlust und Beschädigungen dringend angezeigt. In den nachfolgenden Abschnitten wird auf diese Thematik näher eingegangen um im weiteren Verlaufe dieser Dokumentation ein Datensicherungskonzept zu erarbeiten.

### Sinn & Zweck der Systeme zur Erstellung von Sicherungskopien und Datenwiederherstellung
Der Verlust von Daten kann für ein Unternehmen eine Bedrohung für die eigene Reputation oder gar Existenz darstellen. Einerseits geht ein Datenverlust (sei es durch unbeabsichtigtes Löschen oder durch ein Naturereignis) mit einem gewissen Vertrauensverlust der Kundschaft des betroffenen Unternehmens einher, andererseits kann ein rechtliches Risiko für das betroffene Unternehmen entstehen, da durch Verträge oder rechtliche Vorgaben die sichere und langfristige Aufbewahrung von gewissen Daten (z.B. Finanzunterlagen) verpflichtend ist.

Um diesen Gefahren entgegenzuwirken und die potenziellen Risiken zu minimieren bzw. diesen angemessen zu begegnen, können einige Massnahmen ergriffen werden:

- Regelmässiges Anfertigen von Duplikaten von wichtigen Daten
- Versionshistorie für wichtige Daten, um Änderungen nachvollziehen und falls erforderlich vorherige Versionen wiederherstellen zu können
- Schützen von wichtigen Daten vor dem Überschreiben und Löschen
- Speichern von Datenkopien auf hochverfügbarer Infrastruktur, um schnelle Datenwiederherstellung zu gewährleisten

Eine Massnahme um die nachhaltige Verfügbarkeit von Daten sicherzustellen, wie bereits oben kurz aufgeführt, ist das regelmässige Anfertigen von Sicherungskopien (auch "Backup"): Daten werden also in regelmässigen zeitlichen Abständen (beispielsweise jede Woche) kopiert bzw. dupliziert und die angefertigte Kopie wird auf einem virtuellen oder physischen Datenträger (z.B. externe Festplatte oder Cloud-Speicher) abgelegt. Im Falle einer Beschädigung oder eines Verlustes der primären Daten können mittels der angefertigten Sicherungskopie die von der Beschädigung oder dem Verlust betroffenen Daten wiederhergestellt werden.
Dieses Anfertigen von Sicherungskopien kann selbstredend automatisiert werden und entsprechend finden sich auch viele Angebote auf dem Markt für sogenannte "Backup-Lösungen".


### Betroffene Daten & Infrastruktur-Teile
Im Rahmen dieses Projekts betrachtet EnatLabs ausschliesslich die Datensicherheit von Daten, welche in der birdCloud abgelegt oder verarbeitet werden. Die bestehende On-Premises Infrastruktur von AAL "aapCloud" und die Gewährleistung der Verfügbarkeit und Sicherheit der Daten in der aapCloud sind nicht Gegenstand des Projektes "birdCloud" und somit auch nicht Gegenstand dieser Dokumentation.

Gegenstand dieses Abschnitts "Datenerhaltung & Wiederherstellungsschutzgrad - Backup-Systeme (auch Modul 143)" und des im Laufe dieser Dokumentation folgenden Datensicherungskonzepts ist die gesamte in der Cloud-Umgebung "birdCloud" vorhandene Infrastruktur. Weiter findet [Abschnitt 3.1 der Weisung #AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md#31-wiederherstellungsschutzgrad) Anwendung. Gemäss dem genannten Abschnitt existieren für sämtliche Daten von AAL folgende Wiederherstellungsschutzgrade:

- G1 - NO BACKUP
- G2 - SIMPLE BACKUP
- G3 - DISASTER RESISTANT BACKUP

Die aufgeführten Wiederherstellungsschutzgrade definieren "inwiefern ein Datenobjekt vor Verlust oder Beschädigung geschützt werden soll". Folglich lassen sich je nach Wiederherstellungsschutzgrad andere Massnahmen festlegen, deshalb nachfolgend ein Auszug aus der Weisung #AALD-DC-01:

> **G1 - NO BACKUP**
> 
> Daten mit diesem Grad werden nicht mittels einem Backup gesichert. Der Verlust dieser Daten ist vertretbar und eine Datensicherung würde einen unverhältnismässigen Aufwand darstellen.
> 
> **G2 - SIMPLE BACKUP**
> 
> Daten mit diesem Grad werden mittels einem einfachen Backup gesichert. Folglich wird eine Kopie dieses Datenobjekts angefertigt und in der selben Umgebung abgelegt. Dieses Backup dient lediglich der Wiederherstellung im Falle einer versehentlichen Löschung oder eines Ausfalls eines Datenträgers oder einer bestimmten Cloud-Ressource.
> 
> **G3 - DISASTER RESISTANT BACKUP**
> 
> Daten mit diesem Grad sollen auch im Falle eines weitreichenden Ereignisses noch wiederherstellbar sein. Diese Daten werden mehrfach repliziert und die Kopien in unterschiedlichen Ablageorten abgelegt.

> Das Anfertigen von Sicherungskopien (Backups) soll jederzeit im Einklang mit der entsprechenden Klassifizierungsstufe, welche dem Datenobjekt zugewiesen wurde, stehen. Ist dies nicht möglich, hat die Klassifizierungsstufe Vorrang.

Die oben aufgeführten Anforderungen (entnommen dem [Abschnitt 3.1 der Weisung #AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md#31-wiederherstellungsschutzgrad)) lassen sich, angewandt auf die birdCloud, wie folgt kurz zusammenfassen:

- G1 - NO BACKUP -> Kein Backup
- G2 - SIMPLE BACKUP -> 1 Kopie wird angefertigt und in gleicher Cloud-Region und möglicherweise auch gleicher Availability-Zone abgelegt
- G3 - DISASTER RESISTANT BACKUP -> Eine Kopie wird erstellt, diese vor Löschung/Veränderung geschützt und in mehreren Cloud-Regionen

Von den Bemühungen, die Datensicherheit der Daten der birdCloud sicherzustellen sind folglich nur die Wiederherstellungsschutzgrade "G2 - SIMPLE BACKUP" sowie "G3 - DISASTER RESISTANT BACKUP" betroffen. Daten des Wiederherstellungsschutzgrades "G1 - NO BACKUP" umfassen beispielsweise Messdaten oder Fotografien von Drittunternehmen oder Drittorganisationen (z.B. Bilder des Hubble-Weltraumteleskops bereitgestellt durch die NASA und ESA). Daten dieses Wiederherstellungsschutzgrades werden häufig lediglich zwecks schneller Zugänglichkeit für Mitarbeitende von AAL auf AALs Infrastruktur abgelegt/"zwischengespeichert". Aufgrund dieser Tatsache sieht AAL bei solchen Daten auch die Notwendigkeit, Sicherungskopien anzufertigen, nicht gegeben.

### Nachvollziehbarkeit Sicherungskopie-Versionen
Das Vorhandensein einer Versionshistorie erweist sich bei der Sicherstellung der Datensicherheit als äusserst wichtig, insbesondere um bei einem Verlust von Daten die korrekte zeitliche Abfolge der erstellten Sicherungskopien nachvollziehen und damit eine einwandfreie Wiederherstellung gewährleisten zu können. So kann beispielsweise verhindert werden, dass nach einer Attacke mit Schadsoftware, welche Daten verschlüsselt und den Datenbesitzer damit erpresst (Ransomware), eine Sicherungskopieversion eingespielt wird, welche bereits Bestandteile der schadhaften Software enthält und so zu einer erneuten Infektion der wiederhergestellten Infrastruktur führt.

Aus dem ausgeführten Grunde erachtet es EnatLabs als erforderlich, erstellte Sicherungskopien klar mit Versionsangaben zu kennzeichnen. Die entsprechende technische Umsetzung dieser Kennzeichnung ist Gegenstand des Abschnitts ["Backup-Systeme (auch Modul 143)"](#backup-systeme-auch-modul-143).

### Zu verwendende Dienstleistungen und Applikationen
Zur Umsetzung des, im weiteren Verlaufe dieser Dokumentation definierten, Datensicherungskonzepts, erscheint der Service "AWS Backup" des Cloud-Anbieters AWS besonders geeignet. 

AWS Backup kann die Daten folgender (der im Rahmen des Projektes birdCloud genutzten) AWS-Services sichern:

- Amazon S3 (Simple Storage Service)
- Amazon EBS (Elastic Block Store)
- Amazon EC2 (Elastic Compute Cloud)
- AWS CloudFormation

Die gesicherten Daten werden in einem logischen "Backup-Tresor" ("AWS Backup Vault") abgelegt und mithilfe des für diesen Tresor festgelegten KMS-Keys (AWS Key Management Service (KMS)) verschlüsselt.

> The AWS Backup vault is a logical container that stores and manages your encrypted backups. When creating a backup vault, you must specify the AWS Key Management Service (AWS KMS) encryption key that encrypts the backups placed in this vault. All copied backups are encrypted with the key of the target vault. 

_Quelle: [AWS: AWS Backup Features ](https://aws.amazon.com/backup/features/?nc1=h_ls)_

Ein weiterer Vorteil der Verwendung des Services "AWS Backup" ist die Möglichkeit der Nutzung des Löschschutzes ("AWS Backup Vault Lock")(insbesondere in Hinblick auf die Risiken von Ransomware etc. interessant):

> AWS Backup Vault Lock allows you to protect your backups from deletion or changes to their lifecycle (making data immutable) by inadvertent or malicious changes. You can use the AWS CLI, AWS Backup API, or AWS Backup SDK to apply the AWS Backup Vault Lock protection to an existing vault or a new one. AWS Backup Vault Lock works with backup policies such as retention periods, cold storage transitioning, cross-account, and cross-Region copy. This provides an additional layer of protection and helps meet your compliance requirements.

_Quelle: [AWS: AWS Backup Features ](https://aws.amazon.com/backup/features/?nc1=h_ls)_

### Einklang mit internen Weisungen & rechtlichen Anforderungen
#### Weisung #AALD-DC-01 Abschnitte 2.1
Bei der Anfertigung von Sicherungskopien ist selbstredend die dem zu sichernden Datensatz zugewiesene Klassifizierungsstufe zu berücksichtigen. Die Weisung [#AALD-DC-01](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md) enthält dazu folgende Passage:

> Das Anfertigen von Sicherungskopien (Backups) soll jederzeit im Einklang mit der entsprechenden Klassifizierungsstufe, welche dem Datenobjekt zugewiesen wurde, stehen. Ist dies nicht möglich, hat die Klassifizierungsstufe Vorrang.

Jede angefertigte Kopie eines Datensatzes muss zwingend derselben Klassifizierungsstufe zugewiesen und entsprechend vor unberechtigtem Zugriff geschützt werden. Die Definition des geografischen Standortes (der "Region") ist in Anbetracht der Klassifizierungsstufe "E3 - CONFIDENTIAL" auch für Sicherungskopien relevant. Bei der Planung des Datensicherungskonzepts ist es folglich angezeigt, bei der Betrachtung der Datenhaltung besonderes Augenmerk auf die Vereinbarkeit mit den Anweisungen zum geografischen Standort von Daten der Klassifizierungsstufe "E3 - CONFIDENTIAL" zu legen.

## Theoretische Planung
Die birdCloud wird zwar als Ergänzung zur bereits bestehenden aapCloud in Betrieb genommen, jedoch wird aus Gründen der Segmentierung und Unabhängigkeit auf die Einbindung von bereits vorhandenen Identitäts- und Zugriffverwaltungssystemen aus der aapCloud verzichtet. Selbstredend wird es für AAL mittel- bis langfirstig notwendig sein, weitere Ressourcen, zusätzlich zu den in dieser Dokumentation aufgeführten Dienstleistungen und Ressourcen, in der birdCloud bereitzustellen oder weitere AWS-Services zu nutzen (beispielsweise "Direct Connect" um eine physische Netzwerkverbindung zwischen aapCloud und birdCloud zu nutzen). Folgende grafische Darstellung stellt die [birdCloud in ihrer ersten Version](/Weitere_Ressourcen/Referenz_Dokumente/birdcloud-vers1.pdf) dar. Die birdCloud wird im Rahmen dieses Projekts gemäss Vorgaben, welche aus dieser grafischen Darstellung und weiteren in diesem Abschnitt folgenden Details zu entnehmen sind, umgesetzt.

![Bild birdCloud Aufbau](/Weitere_Ressourcen/Referenz_Dokumente/birdcloud-vers1.drawio.png)

### Aufbau/Struktur birdCloud
Wie bereits im Abschnitt [Datenschutz & Klassifizierungsstufen](#datenschutz-klassifizierungsstufen) erläutert, nutzt birdCloud die beiden AWS-Regions "Nord-Virginia" (us-east-1) und "Frankfurt" (eu-central-1). Die AWS-Region "Nord-Virginia" (us-east-1) dient als primäre Region. Folglich wird das Benutzerverzeichnis des AWS-Services "IAM Identity Center" in dieser Region erstellt und die Verwaltung von Benutzern, Benutzergruppen sowie Berechtigungen erfolgt via diesem AWS-Service.

Wie bereits in der obigen grafischen Darstellung ersichtlich, wird pro Region eine "VPC" (Virtual Private Cloud) erstellt. Innerhalb einer solchen VPC werden EC2-Instanzen (virtuelle Server) sowie weitere AWS-Services einem Subnetz zugeordnet. Die Nutzung von VPCs ist einerseits für bestimmte Services zwingend erforderlich (z.B. EC2 oder Internet Gateway), für andere Services irrelevant (z.B. S3). Die Nutzung von VPCs und von darin befindlichen Subnetzen erlaubt uns die Segmentierung von Ressourcen, was einen Sicherheitsvorteil mit sich bringt. So existieren innerhalb einer VPC private und öffentliche Subnetze. Während öffentliche Subnetze mittels "Internet-Gateway" ans Internet angebunden und die sich in diesem Subnetz befindlichen Ressourcen eine öffentliche IP-Adresse zugeordnet erhalten (IPv4 und optimal IPv6) und somit aus dem Internet bzw. WAN erreichbar sind, sind Ressourcen innerhalb von privaten Subnetzen nur innerhalb dieses einen Subnetzes erreichbar (selbstredend gibt es verschiedene Möglichkeiten, den Zugriff dieser privaten Subnetze auf das Internet / WAN dennoch zu ernöglichen und umgekehrt z.B. mittels eines NAT-Gateways oder einer Rounting-Software auf einer EC2-Instanz mit Anbindung an ein privates und ein öffentliches Subnetz).

#### Benennung von Ressourcen
Bei der Erstellung und Benennung von Ressourcen innerhalb der birdCloud wird wann immer möglich, gemäss Namenskonvention (siehe Abschnitt [Namenskonvention](#namenskonvention)) vorgegangen. Wie bereits im Abschnitt [Datenschutz & Klassifizierungsstufen](#datenschutz-klassifizierungsstufen) erläutert, wird die Klassifizierungsstufe ebenfalls im Namen einer Ressource angegeben. Für Details dazu sei auf den erwähnten Abschnitt verwiesen. Um eine bessere Übersichtlichkeit und Ordnung zu ermöglichen wird die Namenskonvention entsprechend erweitert:

Namenskonvention AWS Cloud (Organization "birdcloud-aerospacelabs-antliuma-com"):
```
birdcloud.aerospacelabs.antliuma.com
birdcloud-aerospacelabs-antliuma-com
birdcloud.aal.antliuma.com
birdcloud-aal-antliuma-com
*.[AWS-Service].[AWS-Region].[Kürzel Wiederherstellungsschutzgrad].[Kürzel Klassifizierungsstufe].birdcloud.aerospacelabs.antliuma.com
*-[AWS-Service]-[AWS-Region]-[Kürzel Wiederherstellungsschutzgrad]-[Kürzel Klassifizierungsstufe]-birdcloud-aerospacelabs-antliuma-com
*.[AWS-Service].[AWS-Region].[Kürzel Wiederherstellungsschutzgrad].[Kürzel Klassifizierungsstufe].birdcloud.aal.antliuma.com
*-[AWS-Service]-[AWS-Region]-[Kürzel Wiederherstellungsschutzgrad]-[Kürzel Klassifizierungsstufe]-birdcloud-aal-antliuma-com
```
Nachfolgend zwei Beispiele für die Benennung von Ressourcen:

`publicimages.std.s3.us-east-1.g1.e1.birdcloud.aerospacelabs.antliuma.com` <br>
`bap1.backupvault.us-east-1.e2.birdcloud.aerospacelabs.antliuma.com`

_Es sei erwähnt, dass bei der Benennung von einigen Ressourcen es nicht unbedingt erforderlich und sinnvoll ist, beispielsweise den Wiederherstellungsschutzgrad anzugeben (Beispiel: Benennung von AWS-Backup-Vaults). In solchen Fällen kann auf einzelne Elemente (hier in der Namenskonvention in eckigen Klammern) verzichtet werden._

#### VPC
Es werden zwei VPCs erstellt. Einerseits eine VPC für die AWS-Region "Nord-Virginia" (us-east-1) und die Region "Frankfurt" (eu-central-1).

VPCs:
- eu-central-1  -> `eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com`
- us-east-1     -> `us-east-1-vpc-birdcloud-aerospacelabs-antliuma-com`

Als private IPv4-Adressen werden folgende für diese Zwecke durch die IANA reservierten IPv4-Adressbereiche verwendet:

- eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com -> `10.35.0.0/16`
- us-east-1-vpc-birdcloud-aerospacelabs-antliuma-com    -> `10.195.0.0/16`

Auf die Verwendung von IPv6-Adressen wird in beiden VPCs verzichtet.
Innerhalb beider VPCs werden jeweils 2 Subnetze erstellt: Ein öffentliches Subnetz und ein privates Subnetz:

- eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com                 -> `10.35.0.0/16`
    - public01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com    -> `10.35.0.0/20`
    - private01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com   -> `10.35.16.0/20`
- us-east-1-vpc-birdcloud-aerospacelabs-antliuma-com                    -> `10.195.0.0/16`
    - public01-us-east-1-vpc-birdcloud-aerospacelabs-antliuma-com       -> `10.195.0.0/20`
    - private01-us-east-1-vpc-birdcloud-aerospacelabs-antliuma-com      -> `10.195.16.0/20`

Die Definition der nicht-öffentlichen IPv4-Adressen ermöglicht das problemlose spätere Hinzufügen von weiteren Subnetzen innerhalb der VPCs und stellt andererseits genügend IPv4-Adressen für Hosts innerhalb der Subnetze zur Verfügung.

Die Erstellung der VPC `eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` und aller sich darin befindlichen Subnetze erfolgt via CloudFormation. Mittels diesem AWS-Service lassen sich Ressourcen innerhalb der AWS-Cloud mittels einer Art von Skript bzw. Parameterdefinition automatisiert erstellen. Zur Erstellug von VPCs stellt AWS selbst eine entsprechende Vorlage für eine solche Datei, welche CloudFormation übergeben wird und CloudFormation infolgedessen die Erstellung der in dieser Datei definierten Ressourcen in die Wege leitet, bereit (siehe [AWS CloudFormation VPC template](https://docs.aws.amazon.com/codebuild/latest/userguide/cloudformation-vpc-template.html)). 

Diese Vorlage wurde entsprechend modifiziert und Änderungen daran vorgenommen damit das VPC `eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` mit dieser [angepassten YAML-Datei](/Weitere_Ressourcen/Skripte/eu-central-1-vpc.yaml) mittels CloudFormation automatisiert erstellt werden kann. Neben den Subnetzen und einem Internet Gateway, um die Anbindung des öffentlichen Subnetzes ans "Internet" bzw. WAN zu ermöglichen, wurden auch drei "Security Groups" erstellt. Diese Firewallregeln definieren, welcher Datenverkehr beispielsweise eine EC2-Instanz erhalten oder versenden kann. 

Nachfolgend sind die drei erstellten Sicherheitsgruppen und deren Eigenschaften kurz aufgelistet:

- MainSecurityGroup (allow-all-inout-sg)
    - Erlaubt jeglichen Datenverkehr von (WAN/Internet & gesamte VPC (0.0.0.0/0) -> z.B. EC2-Instanz) und nach (z.B. EC2-Instanz -> WAN/Internet & gesamte VPC (0.0.0.0/0)) aussen
- InternalSecurityGroup (allow-all-vpc-in-all-out-sg)
    - Erlaubt jeglichen eingehenden Datenverkehr vom VPC "eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com" und allen sich darin befindlichen Subnetzen (gesamte VPC (10.35.0.0/16) -> z.B. EC2-Instanz) und sämtlichen nach aussen gehenden Datenverkehr (z.B. EC2-Instanz -> Internet/WAN & gesamte VPC (0.0.0.0/0))
- StrictSecurityGroup (strict-sg)
    - Erlaubt ausschliesslich ausgehenden Datenverkehr (z.B. EC2-Instanz -> Internet/WAN & gesamte VPC (0.0.0.0/0))


Die VPC `us-east-1-vpc-birdcloud-aerospacelabs-antliuma-com` wird manuell erstellt, jedoch wurde der Aufbau der VPC `eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` kopiert, lediglich die IP-Adressen und die Benennung der Ressourcen (Subnetze etc.) unterscheidet sich.

#### EC2
Insgesamt werden in der birdCloud 3 EC2-Instanzen erstellt sowie eine "Auto Scaling Group".
Nachfolgend aufgelistet die drei EC2-Instanzen sowie die "Auto Scaling Group", den entsprechenden Subnetzen und VPCs hierarchisch untergeordnet:

- us-east-1-vpc-birdcloud-aerospacelabs-antliuma-com 
    - public01-us-east-1-vpc-birdcloud-aerospacelabs-antliuma-com
        - nextcloud.ec2.us-east-1.g2.e2.birdcloud.aerospacelabs.antliuma.com
        - webpage001.ec2.us-east-1.g1.e1.birdcloud.aerospacelabs.antliuma.com

- eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com
    - public01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com
        - vpn.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com (Netwerkschnittstelle 1)
    - private01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com
        - int-nextcloud.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com
        - vpn.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com (Netwerkschnittstelle 2)

##### vpn.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com
Eine EC2-Instanz mit zwei Netzwerkschnittstellen wird erstellt. Eine Netzwerkschnittstelle befindet sich im öffentlichen Subnetz `public01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com`, eine weitere im nicht-öffentlichen Subnetz `private01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com`.

Die erstellte EC2-Instanz mit dem Namen `vpn.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com` dient als VPN (Virtual Private Network)-Server, um Mitarbeitenden von AAL Zugriff auf die Ressourcen, welche sich im nicht-öffentlichen Subnetz `private01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` befinden, zu ermöglichen. Diese zusätzliche Sicherheitshürde schützt die sich im Subnetz `private01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` befindlichen Ressourcen zusätzlich. Ein Zugriff auf diese Ressourcen kann infolge der Eigenschaft des Subnetzes, über kein Internet-Gateway zu verfügen, nur via der so erstellen EC2-Instanz `vpn.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com` erfolgen.

Weiter dient diese EC2-Instanz auch als Router, welcher den Ressourcen innerhalb des nicht-öffentlichen Subnetzes die Kommunikation nach aussen ermöglicht. Dies erfolgt via NAT (Network Address Translation): Der Datenverkehr vom nicht-öffentlichen Subnetz kommend, wird vom Router via seiner Netzwerkschnittstelle 2 empfangen und via Netzwerkschnittstelle 1 ins "Internet"/WAN weitergeleitet. Die privaten IPv4-Adressen der Ressourcen aus dem nicht-öffentlichen Subnetz werden durch die öffentliche IPv4-Adresse, welche dem Router auf Netzwerkschnittstelle 1 zugeordnet ist, ersetzt.

Die Verbindung zwischen den Mitarbeitenden von AAL und dem VPN-Server erfolgt über das OpenVPN-Protokoll und entsprechende Authentifizierung der Mitarbeitenden mittels Zertifikaten bzw. asymetrischer Kryptografie.

Zur Erfüllung dieser Anforderungen an die EC2-Instanz wird die hervorragende Software "RouterOS" des lettischen Unternehmens "MikroTik" eingesetzt. Ein entsprechendes Image (AMI) zur Verwendung auf einer EC2-Instanz wird durch MikroTik auf dem [AWS Marketplace bereitgestellt](https://aws.amazon.com/marketplace/pp/prodview-sf5gn6js6av54?sr=0-1&ref_=beagle&applicationId=AWS-Marketplace-Console). Zur Verwendung der Software ohne Einschränkungen ist der [Kauf einer entsprechenden Lizenz direkt bei MikroTik](https://help.mikrotik.com/docs/display/ROS/Cloud+Hosted+Router%2C+CHR#CloudHostedRouter,CHR-CHRLicensing) vonnöten. In unserem Falle reicht zu Demonstrationszwecken die kostenlose Testlizenz vollends aus.

##### int-nextcloud.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com
Es wird mittels EC2-Instanz, auf welcher das Betriebssystem Debian 12 betrieben wird, eine Umgebung für Docker Conatiner geschaffen, in welcher mehrere Conatiner der freien Software "Nextcloud" betrieben werden. Diese EC2-Instanz befindet sich innerhalb des privaten SUbnetzes `private01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` und kann lediglich via dem Router bzw. VPN-Server `vpn.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com` genutzt werden. 

Alle Nutzer dieser Nextcloud sind angehalten, sämtliche Daten auf dem eingebundenen S3-Bucket zu speichern. Damit werden diese Daten innerhalb der AWS-Cloud gespeichert und die Daten werden durch den Service AWS-Backup gesichert.

##### nextcloud.ec2.us-east-1.g2.e2.birdcloud.aerospacelabs.antliuma.com
Die Umsetzung ist beinahe deckungsleich mit derer der Ec2-Instanz `int-nextcloud.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com`: `nextcloud.ec2.us-east-1.g2.e2.birdcloud.aerospacelabs.antliuma.com` besitzt jedoch eine öffentliche IPv4-Adresse und kann direkt über das Internet gesnutzt werden. Sämtliche Daten werden auf dem an die EC2-Instanz automatisch angebundenen EBS (Elastic Block Storage)-Volumen gespeichert. 

_Aus diesen Gründen wird auf die praktische Umsetzung dieses Teils der birdCloud-Infrastruktur verzichtet_

##### webpage001.ec2.us-east-1.g1.e1.birdcloud.aerospacelabs.antliuma.com
_Auf die praktische Umsetzung der Auto-Scaling-Gruppe zur automatischen Skalierung wurde aus Zeitgründen verzichtet_

#### S3
Insgesamt werden in der birdCloud vier S3-Buckets erstellt. In diesen werden unterschiedliche Daten gespeichert. Diese unterscheiden sich auch in den Klassifizierungsstufen und Wiederherstellungsschutzgraden. Die S3-Buckets werden wie nachfolgend benannt:

- `publicimages.std.s3.us-east-1.g1.e1.birdcloud.aerospacelabs.antliuma.com`
- `documents.std.s3.us-east-1.g3.e2.birdcloud.aerospacelabs.antliuma.com`
- `nextcloud.std.s3.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com`
- `documents.std.s3.eu-central-1.g3.e2.birdcloud.aerospacelabs.antliuma.com`

Dem Namenskonzept der S3-Buckets ist auch zu entnehmen, dass sich zwei der Buckets in der Region us-east-1 und zwei der Buckets in der Region eu-central-1 befinden. Dies aufgrund der Klassifizierungsstufen der entsprechenden Buckets.

Die Buckets `documents.std.s3.eu-central-1.g3.e2.birdcloud.aerospacelabs.antliuma.com` und `documents.std.s3.us-east-1.g3.e2.birdcloud.aerospacelabs.antliuma.com` besitzen den Wiederherstellungsschutzgrad G3 und werden somit mittels Replikation vor Datenverlust geschützt bzw. die Inhalte des Buckets `documents.std.s3.eu-central-1.g3.e2.birdcloud.aerospacelabs.antliuma.com` werden ins Bucket `documents.std.s3.us-east-1.g3.e2.birdcloud.aerospacelabs.antliuma.com` "kopiert". Durch die unterschiedlichen Regions wird damit ein Schutz vor Datenverlust garantiert. Zusätzlich wird mittels Objektsperre verhindert, dass Inhalte des Buckets `documents.std.s3.eu-central-1.g3.e2.birdcloud.aerospacelabs.antliuma.com` versehentlich gelöscht oder überschrieben werden.

#### Route 53
In der birdCloud wird einerseits eine "öffentliche" (public hosted Zone) und eine "nicht-öffentliche" (private hosted Zone) DNS-Zone erstellt. Die Einträge der öffentlichen Zone werden auf verschiedenen DNS-Servern von AWS propagiert. Die Einträge der nicht-öffentlichen DNS-Zone jedoch sind lediglich für Ressourcen innerhalb der VPC `eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` abrufbar. 

Nachfolgend finden sich die Domänen-Namen (domain names) dieser Zonen und die entsprechenden Ressourcen-Namen (dieser wird in der Beschreibung zur Zone vermerkt):

- `ns1priv.route53.eu-central-1.birdcloud.aerospacelabs.antliuma.com`   -> Domain: `*.aal-int.bird`
- `ns1.route53.global.birdcloud.aerospacelabs.antliuma.com`             -> Domain: `*.bc.aerospacelabs.antliuma.com`

#### CloudFront
CloudFront nennt sich AWS "Content Delivery Network", welches Webinhalte zwischenspeichert und an verschiedenen geografischen Standorten ablegt und im Falle eines Aufrufs der Webinhalte diese aus dem dem Nutzer geografisch nähest gelegenen Standortes bezieht. Folglich hat dies verkürzte Ladezeiten für den Ntzer zur Folge.

Die Webseite "valentin-43-44.org", aufrufbar unter der entsprechenden URL `http://valentin-43-44.org/` wird zu Demonstrationszwecken der Möglichkeiten des Services CloudFront verwendet. Unter `http://valentin-43-44.org/` ist lediglich eine "Parking page" aufrufbar, für unsere Zwecke wird dies jedoch genügen.

Die Webinhalte unter `http://valentin-43-44.org/` werden von CloudFront abgerufen und zwischengespeichert. Da wir eine möglichst hochverfügbare Webseite für die Nutzer bereitstellen möchten, wir die weltweite Verteilung der Inhalte in sämtliche AWS-"Edge"-Standorte angestrebt.
 
#### IAM
Für sämtliche Mitarbeitende von AAL werden persönliche AWS Identitäten erstellt bzw. mittels AWS IAM Identity Center werden Benutzeraccount für jeden Mitarbeitenden von AAL erstellt. Die Berechtigungen werden mittels Benutzergruppen definiert. Dies ermöglicht das schnelle Hinzufügen und Entfernen von Benutzern, ohne jeweils pro Benutzer Berechtigungen zuweisen zu müssen.
Im Sinne des "Open-Book"-Modells werden allen Mitarbeitenden von AAL folgende Berechtigungen erteilt:

Berechtigungssatz/Permission set "aal-standarduser-internal": 
- AmazonEC2FullAccess
- AmazonRoute53DomainsFullAccess
- AmazonRoute53FullAccess
- AmazonRoute53ResolverFullAccess
- AmazonS3FullAccess

Es fällt auf, dass relativ grosszügige Berechtigungen erteilt werden, dazu sei aber erwähnt, dass einerseits die Transparenz innerhalb von AAL einen hohen Stellenwert besitzt und andererseits mit den aufgeführten Berechtigungssätzen ("AWS managed policies") lediglich Zugriff auf die von allen AAL-Mitarbeitenern genutzten AWS-Services erlaubt wird. So wird Zugriff beispielsweise auf die VPCs nicht via diesem so erstellten Berechtigungssatz (Permission set) ermöglicht.


Ein zweiter Berechtigungssatz (Permission set) wird für AAL Mitarbeitende mit dem Security Level TS5 erstellt. Dieser enthält folgende "AWS managed policies":

Berechtigungssatz/Permission set "aal-securitylevel-ts5-people":
- AdministratorAccess
- AWSBillingConductorFullAccess

Dieser Berechtigungssatz ermöglicht die Administration der birdCloud durch Mitarbeitende mit Security Level TS5. 

### Backup-Systeme (auch Modul 143)
#### Datensicherungskonzept

##### Bedrohungsmodell
Das Anfertigen von Sicherungskopien ("Backups") verfolgt das Ziel, Daten vor dem absichtlichen oder unabsichtlichen Verlust oder der absichtlichen oder unabsichtlichen Beschädigung zu schützen. 

- Es soll verhindert werden, dass böswillige Aktuere (seinen es interne Mitarbeitende oder Dritte) wichtige Daten, welche für AAL relevant sind, beschädigen oder gar vernichten.
- Es soll weiter verhindert werden, dass wichtige Daten, welche für AAL relevant sind, unabsichtlich (beispielsweise durch Mitarbeitende von AAL) beschädigt oder vernichtet werden.
- Es soll ebenso verhindert werden, dass bei einem Befall durch Schadsoftware der internen Infrastruktur von AAL, keine Datenwiederherstellung möglich ist, da bereits in den Datensicherungen die Schadsoftware vorhanden ist und somit eine erneute Infektion der internen Infrastruktur zur Folge haben würde
- Es soll ebenso verhindert werden, dass wichtige Daten für Mitarbeitende von AAL unzugänglich gemacht werden

##### Definition "Relevanter" resp. "Wichtiger" Daten
Zur Einstufung von Daten zwecks Massnahmendefintion zum Schutze vor Datenverlust, exitsiert die [Weisung #AALD-DC-01 zur Datenklassifizierung](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md). Diese definiert im Abschnitt 3.1 verschiedene Wiederherstellungsschutzgrade, welche entscheiden, inwiefern Massnahmen zum Schutze vor Verlust der eingestuften Daten getroffen werden müssen. 
Folglich werden sämtliche Daten innerhalb von AAL gemäss dieser Weisung klassifiziert und jedem Datensatz wird eine Klassifizierungsstufe sowie ein Wiederherstellungsschutzgrad zugewiesen. 

Nachfolgend findet sich eine Auswahl an Daten und der jeweilig zugewiesene Wiederherstellungsschutzgrad:

- Sämtliche Daten über Mitarbeitende werden dem Wiederherstellungsschutzgrad G2 zugeordnet.
- Messdaten und andere Daten angefertigt von AAL-Satelliten werden dem Wiederherstellungsschutzgrad G2 zugeordnet
- Sämtliche Daten über die Organisation AAL werden dem Wiederherstellungsschutzgrad G3 zugeordnet
    - Abrechungen, Finanzunterlagen
    - Aufzeichnungen zum Aufbau der internen Infrastruktur
    - Systemkonfigurationen etc.
- Sämtliche Daten welche Vertragsbeziehungen oder Dritte zum Inhalt haben werden dem Wiederherstellungsschutzgrad G2 zugeordnet
- Sämtliche Daten über AAL-Satelliten oder anderer Infrastruktur von AAL welche sich momentan in der Luft oder im interstellaren Raum befinden, sind dem Wiederherstellungsschutzgrad G3 zugeordnet
- Schriftverkehr mit Dritten werden dem Wiederherstellungsschutzgrad G2 zugeordnet
- Interner Schriftverkehr wird dem Wiederherstellungsschutzgrad G1 zugeordnet

Bei Unklarheiten ist die Compliance-Abteilung von AAL anzurufen. Wiedersprechen sich oben genannte Zuordnungen, so hat die Zuordnung mit dem höheren Wiederherstellungsschutzgrad Vorrang (G3 > G2 > G1).

##### Definition Massnahmen für Wiederherstellungsschutzgrade

_Auszug aus der [Weisung #AALD-DC-01 zur Datenklassifizierung](https://gitlab.com/vb-school/modul_346/-/blob/main/corporate_docs/datenklassifizierung.md):_
```
G1 - NO BACKUP

Daten mit diesem Grad werden nicht mittels einem Backup gesichert. Der Verlust dieser Daten ist vertretbar und eine Datensicherung würde einen unverhältnismässigen Aufwand darstellen.

G2 - SIMPLE BACKUP

Daten mit diesem Grad werden mittels einem einfachen Backup gesichert. Folglich wird eine Kopie dieses Datenobjekts angefertigt und in der selben Umgebung abgelegt. Dieses Backup dient lediglich der Wiederherstellung im Falle einer versehentlichen Löschung oder eines Ausfalls eines Datenträgers oder einer bestimmten Cloud-Ressource.

G3 - DISASTER RESISTANT BACKUP

Daten mit diesem Grad sollen auch im Falle eines weitreichenden Ereignisses noch wiederherstellbar sein. Diese Daten werden mehrfach repliziert und die Kopien in unterschiedlichen Ablageorten abgelegt.
```
Nachfolgend werden im Detail die Massnahmen pro Wiederherstellungsschutzgrad definiert:

**G1 - NO BACKUP** <br>
Keine Massnahmen, da Daten entbehrlich sind. 

**G2 - SIMPLE BACKUP** <br>
Backup und Schutz vor Veränderung mittels Service "AWS Backup". AWS Backup erstellt eine Sicherungskopie und sichert diese in einem enstprechenden "Backup-Vault". Dieser "Backup-Vault" kann sich in derselben Region und Availability Zone wie die zu sichernde Ressourcen befinden.

**G3 - DISASTER RESISTANT BACKUP** <br>
Für Daten in S3-Buckets:
- Backup und Schutz vor Veränderung durch Nutzung von S3 Objektsperre (Compliance-Modus) und Service AWS Backup. Daten werden mittels S3 Objektsperre und dazugehöriger Versionshistorie vor Veränderungen (absichtlichen sowie unabsichtlichen) sowie vor Löschungen (absichtlichen sowie unabsichtlichen) geschützt. Weiter wird das entsprechende S3-Bucket mittels AWS Backup erneut gesichert. 
Für Daten ausserhalb von S3-Buckets:
- Backup mittels AWS-Backup. Sicherungskopien werden in zwei unterschiedlichen Regionen gespeichert.

##### Häufigkeit Backup-Erstellung und Aufbewahrungsdauer
- G1 - NO BACKUP -> Keine Sicherungskopieanfertigung
- G2 - SIMPLE BACKUP -> Backup wird täglich angefertigt und zwei Monate aufbewahrt 
- G3 - DISASTER RESISTANT BACKUP
    - Daten in S3-Buckets -> Objektsperre für mindestens 48 Monate bei Hochladen/Ablegen der Objekte im S3-Bucket, Backup wird via AWS Backup alle 12 Stunden angefertigt und drei Monate aufbewahrt
    - Daten ausserhalb von S3-Buckets -> Backup wird alle 12 Stunden angefertigt und sechs Monate aufbewahrt


## Praktische Umsetzung
Nachfolgend wird die theoretische Planung parktisch umgesetzt. Die parktische Umsetzung findet innerhalb eines AWS-Kontos statt, welches als zusätzliche Organisation einem bereits bestehenden AWS-Konto angefügt wird. Folglich werden die Berechtigungen weiterhin über das Identitätsmanagement des übergeordneten Kontos verwaltet. Weiter wird auf das Volumen wie in der vorangehenden theoretischen Planung teilweise verzichtet (z.B. werden günstigere EC2-Instanzen-Typen gewählt oder die Aufbewahrungszeiträume von Daten (S3 Objektsperrung) werden bewusst als äusserst kurze Zeiträume definiert). Dennoch bildet die nachfolgende parktische Umsetzung gut ab, inwiefern das Konzept birdCloud erfolgreich umsetzbar ist.

![sandboxaccount](/Weitere_Ressourcen/Bilder/sandbox-account-org-set.png)
![sandbox-login](/Weitere_Ressourcen/Bilder/login-enatlabs-aal-sanboxaccount.png)

### Einrichtung und Konfiguration VPC
#### eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com
Zur Erstellung dieser VPC (Virtual Private Cloud) erfolgte via CloudFormation, ein AWS-Service zur automatisierten Erstellung von Ressourcen und Infrastrukturteilen. Die entsprechende Datei, welche an CloudFormation übergeben wurde, ist [hier](https://gitlab.com/vb-school/modul_346/-/blob/main/Weitere_Ressourcen/Skripte/eu-central-1-vpc.yaml) zu finden. Die YAML-Datei wurde vorgängig im S3-Bucket `documents.std.s3.eu-central-1.g3.e2.birdcloud.aal.antliuma.com` abgelegt und die entsprechende URL kopiert: `https://s3.eu-central-1.amazonaws.com/documents.std.s3.eu-central-1.g3.e2.birdcloud.aal.antliuma.com/INFRASTRUCTURE_DOCUMENTS_AAL_BIRDCLOUD/eu-central-1-vpc.yaml`. Diese konnte im Anschluss an CloudFormation übergeben werden.

Bei der Erstellung des sogenannten "Stacks" in CloudFormation ist darauf zu achten, dass wir uns in der korrekten Region (eu-central-1) befinden. 

![Create Stack 1](/Weitere_Ressourcen/Bilder/createstack-cloudformation-1.png)

Im nächsten Schritt können nocheinmals die Angaben zur Bennenung etc. kontrolliert werden.

![Create Stack 2](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_094510.png)

Bei der Definiton der Vorgehensweise beim Auftreten eines Fehlers wählen wir die vollständige Rückgängigmachung aller erfolgter Handlungen an (complete Rollback).

![create stack 3](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_094726.png)

Sobald die Konfiguration unseres Stacks abgeschlossen ist, wird dieser ausgeführt und wir können den aktuellen Status mitverfolgen.

![STatus CF](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_094959.png)

Sobald uns CloudFormation den Abschluss des Stacks signalisiert, kontrollieren wir innerhalb der Übersicht zu unseren VPCs, ob unser neu erstelltes VPC in der Region eu-central-1 vorhanden ist:

![check vpc](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_095142.png)

Wir sehen ebenfalls die erstellten Subnetze sowie die entsprechenden Routing-Tabellen. Die Ausführung unseres CloudFormation-Stacks war folglich erfolgreich und das VPC `eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` vollständig erstellt wurde.

![Subnetze](/Weitere_Ressourcen/Bilder/subnetze.png)

#### us-east-1-vpc-birdcloud-aerospacelabs-antliuma-com
Das VPC `us-east-1-vpc-birdcloud-aerospacelabs-antliuma-com` wird manuell erstellt, die entsprechenden Angaben sind der theoretischen Planung innerhalb dieser Dokumentation zu entnehmen.

![Manuell VPC](/Weitere_Ressourcen/Bilder/create-vpc-manuell.png)
![Manuell VPC](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_100717.png)

### Einrichtung und Konfiguration EC2
#### vpn.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com
Es wird eine EC2-Instanz erstellt, auf welcher das Betriebssystem RouterOS läuft. Dieser Instanz werden zwei Netzwerkschnittstellen angefügt. Diese Instanz wird als OpenVPN Server konfiguriert und ermöglicht es so, sich mittels OpenVPN Client mit diesem Router/EC2-Instanz zu verbinden. Damit ist es von nun an auch möglich, EC2-Instanzen welche sich innerhalb des privaten Subnetzes `private01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` befinden, "anzupingen" oder sich mit diesen via SSH zu verbinden.

![2 NICs](/Weitere_Ressourcen/Bilder/zwei_ips.png)
![OVPN Client](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_121001.png)
![ping](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_121340.png)
![SSH over VPN](/Weitere_Ressourcen/Bilder/ssh-vpn.png)

In der Routing-Tabelle des privaten Subnetzes `private01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` wird entsprechend definiert, dass sämtlicher Datenverkehr, welcher nicht fürs lokale Subnetz bestimmt ist, an unseren Router (vpn.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com) weitergeleitet werden.

`0.0.0.0/0 VIA [Netzwerkschnittstelle privates Subnetz von vpn.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com]`

#### int-nextcloud.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com
Es wird eine EC2-Instanz mit Debian erstellt, welche sich im privaten Subnetz `private01-eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` befindet. Sämtliche Kommunikation der Instanz nach aussen sowie von aussen (WAN) zur Instanz hin, erfolgt via unserem Router (vpn.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com). 

![erfolgtooutside](/Weitere_Ressourcen/Bilder/accesstooutworld.png)

Besteht eine Verbindung zum weltweiten Web (WAN), kann ein vorbereitetes Skript ausgeführt werden. Dieses installiert unter anderem die Docker Engine und lässt mehrere Container der freien Software "Nextcloud" auf dieser EC2-Instanz (int-nextcloud.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com) laufen. 

![skirpt](/Weitere_Ressourcen/Bilder/skriptintnext.png)

Nun kann via einem Client, welcher mittels OpenVPN mit unserem Router/VPN-Server verbunden ist, auf die Weboberfläche von Nextcloud zugreifen.

![nextcloudwbeint](/Weitere_Ressourcen/Bilder/nextcloudintbeweis.png)

#### nextcloud.ec2.us-east-1.g2.e2.birdcloud.aerospacelabs.antliuma.com
Die Umsetzung ist beinahe deckungsleich mit derer der Ec2-Instanz `int-nextcloud.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com`: `nextcloud.ec2.us-east-1.g2.e2.birdcloud.aerospacelabs.antliuma.com` besitzt jedoch eine öffentliche IPv4-Adresse und kann direkt über das Internet gesnutzt werden. Sämtliche Daten werden auf dem an die EC2-Instanz automatisch angebundenen EBS (Elastic Block Storage)-Volumen gespeichert. 

_Aus diesen Gründen wird auf die praktische Umsetzung dieses Teils der birdCloud-Infrastruktur verzichtet_

### Einrichtung und Konfiguration S3
#### publicimages.std.s3.us-east-1.g1.e1.birdcloud.aerospacelabs.antliuma.com
Bei der Einrichtung des S3-Buckets `publicimages.std.s3.us-east-1.g1.e1.birdcloud.aerospacelabs.antliuma.com` wurden sämtliche Einstellungen auf der Standardkonfiguration belassen. Details sind den Bildschrimfotos zu entnehmen:

![create s3](/Weitere_Ressourcen/Bilder/s3-publicimages.std.s3.us-east-1.g1.e1.birdcloud.aerospacelabs.antliuma.com-create.png)
![create s3 2](Weitere_Ressourcen/Bilder/s3publicimages.std.s3.us-east-1.g1.e1.birdcloud.aerospacelabs.antliuma.com-2.png)

(Aufgrund einer maximalen Zeichenbeschränkung von 63 zeichen wurde der Name des S3-Buckets wie folgt gekürzt: `publicimages.std.s3.us-east-1.g1.e1.birdcloud.aal.antliuma.com`)
In diesem S3-Bucket werden Bilder (beispielsweise von Weltraumobjekten) abgelegt, welche der Öffentlichkeit zugänglich sein sollen. Aufgrund dieser Tatsache werden im Anschluss an die Erstellung des Buckets entsprechende Änderungen an der Konfiguration vorgenommen:

Einerseits wurde das Blockieren des gesamten öffentlichen Zugriffs deaktiviert. Im Anschluss daran wird die Bucket-Richtlinie bearbeitet um den Zugriff ohne jede Authentifizierung zu ermöglichen:

![Allow public](/Weitere_Ressourcen/Bilder/publicaccess-publicimages.std.s3.us-east-1.g1.e1.birdcloud.aerospacelabs.antliuma.com.png)
![Allow pullic policy](/Weitere_Ressourcen/Bilder/allowalaccess3publicimages.png)

![erfolg](/Weitere_Ressourcen/Bilder/s3-publicimages-erfolgreich.png)


#### documents.std.s3.eu-central-1.g3.e2.birdcloud.aerospacelabs.antliuma.com
Dieses S3-Bucket wird erstellt zur Ablage von Dokumenten und Daten mit klassifizierungsstufe E2 oder niedriger. Für dieses Bucket wird die sogenannte Objektsperre aktiviert. Damit können Objekte innerhalb dieses Buckets vor Überschreiben und Löschen geschützt werden und sind damit auch vor Gefahren durch interne Mitarbeitende von AAL geschützt.
(Aufgrund einer maximalen Zeichenbeschränkung von 63 zeichen wurde der Name des S3-Buckets wie folgt gekürzt: `documents.std.s3.eu-central-1.g3.e2.birdcloud.aal.antliuma.com`).

![Erstellung Bucket 1](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07082354.png)
![Erstellung Bucket 2](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07082409.png)

Nach der Erstellung des Buckets (und zwingend erfolgter Aktivierung der Objektsperrfunktion des Buckets) können die genauen Eigenschaften der Objektsperre konfiguriert werden. Wir aktivieren die "Standardaufbewahrung" und definieren so die Objektsperreigenschaft automatisch für sämtliche Objekte, welche neu dem Bucket hinzugefügt werden. Als Standardaufbewahrungsmodus (Objektsperrmodus, welcher automatisch auf neu hochgeladene Objekte angewendet wird) wurde die Ausführung "Compliance" gewählt. Während der Dauer der Objektsperre im Modus "Compliance" kann ein Objekt durch keinen Nutzer gelöscht oder überschrieben werden. Selbst der Inhaber der AWS-Organisation kann das Objekt ausschliesslich durch die Löschung bzw. Schliessung der gesamten AWS-Organisation bzw. des gesamten AWS-Kontos erreichen. Im Screenshot ist ersichtlich, dass als standardmässige Dauer für die Objektsperre "1 Tag" definiert wurde, selbstredend würde in einer Produktivumgebung eine längere Dauer der Objektsperre gewählt. Anzumerken ist auch, dass die Dauer der Objektsperre auch im Nachhinein noch verlängert jedoch nicht verkürzt werden kann.

![Objektsperre Standardaufbewahrung](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_082448.png)
![Objektsperre uebersicht](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_082528.png)

Asbald jegliche vorhergehenden Konfigurationsanpassungen vorgenommen wurden, kann die Erstellung einer [Replikationsregel](https://docs.aws.amazon.com/AmazonS3/latest/userguide/replication.html?icmpid=docs_amazons3_console) versucht werden. Die Erstellung wird jedoch fehlschlagen, da bei aktivierter Objektsperre innerhalb des Buckets manuelle Unterstützung durch den AWS-Support erforderlich ist.
![Repli Error](/Weitere_Ressourcen/Bilder/repli-error.png)

An dieser Stelle verzichte ich auf die Kontaktaufnahme mit dem AWS-Support, es sei jedoch angemerkt, dass die Erstellung von Replikationsregeln, welche sämtliche Objekte aus dem Bucket `documents.std.s3.eu-central-1.g3.e2.birdcloud.aerospacelabs.antliuma.com` ins Bucket `documents.std.s3.us-east-1.g3.e2.birdcloud.aerospacelabs.antliuma.com` kopieren, prinzipliell möglich ist. Diese Replikation über Regionsgrenzen hinweg (Cross-Region Replication) garantiert eine bessere Haltbarkeit der Daten. Nachfolgend wird lediglich ein Screenshot zu Demonstrationszwecken der Replikationsfunktion angewendet auf ein Testweise erstelltes S3-Bucket gezeigt:

![Test repli](/Weitere_Ressourcen/Bilder/create-repli-test-1.png)
![Test repli 2](/Weitere_Ressourcen/Bilder/create-repli-test-2.png)
![Test repli 3](/Weitere_Ressourcen/Bilder/create-repli-test-3.png)

#### documents.std.s3.us-east-1.g3.e2.birdcloud.aerospacelabs.antliuma.com
Dieses S3-Bucket wird erstellt zur Ablage von Dokumenten und Daten mit klassifizierungsstufe E2 oder niedriger. Für dieses Bucket wird die sogenannte Objektsperre aktiviert. Damit können Objekte innerhalb dieses Buckets vor Überschreiben und Löschen geschützt werden und sind damit auch vor Gefahren durch interne Mitarbeitende von AAL geschützt.
Da diesselben Einstellungen wie im vorher erstellten Bucket `documents.std.s3.eu-central-1.g3.e2.birdcloud.aerospacelabs.antliuma.com` bereits entsprechend bei der Erstellung angegeben, verwendet werden, können diese Einstellungen entsprechend von dem erwähnten Bucket übernommen werden.
(Aufgrund einer maximalen Zeichenbeschränkung von 63 zeichen wurde der Name des S3-Buckets wie folgt gekürzt: `documents.std.s3.us-east-1.g3.e2.birdcloud.aal.antliuma.com`).

![Erstellung s3 bucket 1](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_082637.png)

Nach Erstellung des Buckets müssen noch weitere Details der Objektsperre definiert werden. So wird, wie nachfolgend im Screenshot ersichtlich, die Standardaufbewahrung aktiviert und die benötigten Angaben dazu angegeben:

![objektsperre useast1](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_082706.png)


#### nextcloud.std.s3.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com
Dieses S3-Bucket dient der Speicherung von Daten via der internen NextCloud (`int-nextcloud.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com`). (Aufgrund einer maximalen Zeichenbeschränkung von 63 zeichen wurde der Name des S3-Buckets wie folgt gekürzt: `nextcloud.std.s3.eu-central-1.g2.e3.birdcloud.aal.antliuma.com`).
Auf die Konfiguration einer Objektsperre wird in diesem S3-Bucket verzichtet (Der Wiederherstellungsschutzgrad G2 dieses Buckets erlaubt die einfache Sicherungskopieanfertigung und fordert keine ausserordnetlichen Massnahmen zum Schutze der Daten vor Verlust).

![Erstellung s3](/Weitere_Ressourcen/Bilder/bucketnextcloudcretateint.png)
![Erstellung S3 2](/Weitere_Ressourcen/Bilder/bucketnetxtcloudcreate2.png)


### Einrichtung und Konfiguration Route 53
Es werden zwei Zonen erstellt. Eine "private"/Nicht-öffentliche sowie eine öffentliche Zone, welche weltweit durch AWS propagiert wird. 

![private](/Weitere_Ressourcen/Bilder/route53-private-1.png)
![private](/Weitere_Ressourcen/Bilder/route53-private-2.png)
![public 53](/Weitere_Ressourcen/Bilder/route53-public.png)
![piublic domain](/Weitere_Ressourcen/Bilder/dnsaeintragfurdnsserver.png)
![domain](/Weitere_Ressourcen/Bilder/dns.png)

Die private/nicht-öffentliche DNS-Zone ist aus dem privaten Subnetz `private01-eu-central-
1-vpc-birdcloud-aerospacelabs-antliuma-com` abfragbar, wie nachfolgender Screenshot (gemacht auf der EC2-Instanz `int-nextcloud.ec2.eu-central-1.g2.e3.birdcloud.aerospacelabs.antliuma.com`) belegt:

![dnslokal](/Weitere_Ressourcen/Bilder/dnslokal.png)

### Einrichtung und Konfiguration CloudFront
Bei der Konfiguration der CloudFront "Verteilung" (Distribution) wird als "Orgin domain" der "domain name" `valentin-43-44.org` definiert. Im Abschnitt "Default cache behavior" wird die "Viewer protocol policy" auf "Redirect HTTP to HTTPS" gesetzt. Damit werden AUfrufe von Nutzern via HTTP automatisch auf HTTPS umgeleitet. Damit wird eine bessere Sicherheit vor Manipulation der Webinhalte gewährleistet. Es wäre selbstredend jedoch empfehlenswert, wenn die urpsrüngliche Webseite ebenfalls via HTTPS aufrufbar wäre, was momentan nicht der Fall ist. Dabei besteht zwar zwischen Webseitenaufrufer und AWS CloudFront keine Gefahr bzw. eine geringere Gefahr der Manipulation der Webinhalte, jedoch sind die Webinhalte welche von `http://valentin-43-44.org/` selbst zu AWS hin übertragen werden, nicht vor Manipulation geschützt, da die Inhalte weiterhin via HTTP übertragen und nicht mittels TLS-Verschlüsselung geschützt werden.

Im Abschnitt "Settings" kann die "Price class" definiert werden. Hierbei wird die Option "Use all edge locations (best performance)" gewählt um eine Verteilung der Webinhalte über den gesamten Globus zu erreichen und damit kürzere Ladezeiten bei Aufruf der CloudFront Verteilung durch Nutzer zu erreichen.

Ist die CloudFront Verteilung erstellt, kann nun via `https://d2y7hg1loc1pv4.cloudfront.net` der "gespiegelte" Webinhalt betrachtet werden.

![create cloudfront](/Weitere_Ressourcen/Bilder/cloudfront-1.png)
![create cloudfront 2](/Weitere_Ressourcen/Bilder/cloudfront-2.png)
![create cloudfront 3](/Weitere_Ressourcen/Bilder/cloudfonrt-3.png)

Nachfolgend ist ersichtlich, dass die Inhalte von `http://valentin-43-44.org/` erfolgreich via CloudFront (`https://d2y7hg1loc1pv4.cloudfront.net`) aufgerufen werden können.

![webinhalte](/Weitere_Ressourcen/Bilder/cloudfonrt-erfolgreich.png)

Mittels dem Terminal-Webwerkzeug `curl` konnte weiter geprüft werden, ob HTTP-Anfragen automatisch auf die verschlüsselte Version HTTPS umgestellt bzw. weitergeleitet werden (wie vorgängig bei der Erstellung der CloudFront Verteilung definiert).

![curl](/Weitere_Ressourcen/Bilder/moved-cloudfront-httptohttps.png)


### Einrichtung und Konfiguration IAM
Das Zuweisen von Berechtigungen um Zugriff auf die Ressourcen der birdCloud über die sogenannte "Webmanagementkonsole" von AWS oder über andere Zugriffswege (AWS CLI etc.) zu erhalten, ist es notwendig einen IAM-Benutzeraccount mit Zugriff auf die Ressourcen der birdCloud zu besitzen. Diese IAM-Benutzeraccounts können via dem AWS Service "IAM Identity Center" erstellt und verwaltet werden. Um eine übersichliche Verwaltung zu gewährleisten ist es empfehlenswert eine Einteilung in Benutzergruppen vorzunehmen und diesem Gruppen Berechtigungen zu erteilen. 

### Einrichtung und Konfiguration Backup-System (auch Modul 143)
Es wurde ein Backup-Plan für die Daten des Wiederherstellungsschutzgrades "G2" erstellt:

![daily2mon-1-create](/Weitere_Ressourcen/Bilder/daily2mon.png)
![daily2mon-2-create](/Weitere_Ressourcen/Bilder/daily2mon-2.png)

Bei der Zuweisung der Ressourcen, welche gesichert werden sollen durch unseren vorhergehenden Backup-Plan, konnte ich alle Ressourcen inkludieren ausser die Namen dieser Ressourcen enthalten die Buchstabenfolgen "g1" oder "g3". Denn diesen Buchstabenfolgen im Namen einer Ressource signalisieren die Zugehörigkeit zu einem anderen Wiederherstellungsschutzgrad und dürfen somit nicht in die Datensicherung für G2-Daten miteinfliessen.
![incl. all without s1 and s3](/Weitere_Ressourcen/Bilder/daily2monallg2.png)

Ähnlich verfahren wir auch bei der Erstellung der Backup-Pläne und Ressourcenzuweisungen für die Daten der anderen Wiederherstellungsschutzgrade. Beim Wiederherstellungsschutzgrad G3 (Daten ausserhalb von S3-Buckets) ist jedoch darauf zu achten, dass entsprechend konfiguriert sein muss, dass die erstellte Sicherungskopie auch in einer zusätzlichen Region (in unserem Falle us-east-1) abgelegt wird und somit eine geografische Redundanz gegeben ist.

![G3 S3 Data](/Weitere_Ressourcen/Bilder/g3-s3-ress-1.png)
![G3 S3 Data 2](/Weitere_Ressourcen/Bilder/g3-s3-ress-2.png)

![nons3](/Weitere_Ressourcen/Bilder/g3-nons3-1.png)
![nons3](/Weitere_Ressourcen/Bilder/g3-nons3-2.png)
![nons3](/Weitere_Ressourcen/Bilder/g3-non-s3-3.png)

![gs3s3](/Weitere_Ressourcen/Bilder/g3-s3-1.png)
![gs3s3](/Weitere_Ressourcen/Bilder/g3-s3-2.png)

Um Benachrichtigungen über nicht erfolgreiche Backups mit AWS Backup zu erhalten, kann mittels dem AWS Service "SNS" ein neuer Nachrichtenkanal erstellt werden. Im Anschluss daran wird via AWS CLI folgender Befehl ausgeführt:

`aws backup put-backup-vault-notifications --endpoint-url https://backup.eu-central-1.amazonaws.com --backup-vault-name [VAULTNAME] --sns-topic-arn [ARN SNS KANAL] --backup-vault-events BACKUP_JOB_COMPLETED`

Wird nun noch unser SNS-Nachrichtenkanal abonniert und als Filter für die Benachrichtungen folgendende JSON-Angaben angegeben, werden wir von nun an beispielsweise per Mail über nicht-erfolgreiche Backups bzw. Vorgänge innerhalb unseres spezifizierten Backup-Vaults informiert:

```
{
  "State": [
    {
      "anything-but": "COMPLETED"
    }
  ]
}
```

![sns](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_162640.png)
![sns thema](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_162742.png)
![sns abo](/Weitere_Ressourcen/Bilder/Screenshot_2023-11-07_162945.png)

In der AWS Region us-east-1 wird der AWS Service "AWS Backup" gemäss theoretischer Planung ebenfalls konfiguriert. Aus Zeitgründen wird an dieser Stelle jedoch auf die praktische Umsetzung desselben verzichtet. Die Vorgehensweise ist in weiten Teilen deckungsleich mit derer in der Region eu-central-1.

### Möglichkeiten einer Migration von Cloud A zu Cloud B
Aufgrund der Tatsache, dass das VPC `eu-central-1-vpc-birdcloud-aerospacelabs-antliuma-com` über CloudFormation via YAML-Datei erstellt wurde, sollte es ermöglichen, die benötigten Angaben für eine Neuerstellung des VPCs aus dieser Datei zu überführen und manuell in einer anderen Cloud-Umgebung umzusetzen. 


## Schlussfazit & Handlungsempfehlung
EnatLabs anerkennt, dass die im Rahmen dieser Dokumentation praktisch umgesetzten Infrastrukturteile keineswegs für die produktive Nutzung von AAL geeignet sind. Die im Rahmen dieser Dokumentation praktische Umsetzung hatte einzig und allein den Sinn und Zweck, aufzuzeigen, dass die entworfene Infrastruktur "birdCloud" auch praktisch umsetzbar und schlussendlich nutzbar ist. 

EnatLabs empfiehlt AAL zusätzlich zu der bereits vorhandenen Einteilung von Daten in Wiederherstellungsschutzgrade, eine zusätzliche Klassifizierung von Daten aufgrund der Häufigkeit der Veränderung an den Daten vorzunehmen. Dies würde es AAL ermöglichen, Funktionen innerhalb der birdCloud wie S3 Glacier zu nutzen, um Daten, welche sehr selten bis gar nicht verändert werden, in die enstsprechenden Speicherklassen zu verschieben. Damit würde AAL Kosten einsparen und gleichzeitig die Einhaltung gesetzlicher Vorschriften garantieren.

Abschliessend empfiehlt EnatLabs AAL dringend, die aktuellen Abläufe und Prozesse zur Anfertigung von Sicherungskopien (auch "Backups") der in der aapCloud abgelegten Daten zu überarbeiten. Von der jetztigen Vorgehensweise einer manuellen Anfertigung der Sicherungskopien und das anschliessende Ablegen am selben geografischen Standort, an welchem auch das Datencenter der aapCloud zu finden ist, rät EnatLabs aufgrund der fehlenden Redundanz sowie weiteren Risiken dringend ab.
